package com.sparwk.communicationnode.noti.biz.v1.noti;

import com.sparwk.communicationnode.noti.biz.v1.noti.model.Notification;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
public class NotiController {

    @Autowired
    NotiService notiService;

    @GetMapping(path = "/test/{profileId}", produces = MediaType.TEXT_EVENT_STREAM_VALUE)
    public Flux<String> test (@PathVariable(name = "profileId")String profileId){
        return Flux.just("");
    }

    @GetMapping(path = "/test")
    public Mono<Notification> test1 (@RequestBody Notification dto){
        return Mono.just(dto);
    }

}
