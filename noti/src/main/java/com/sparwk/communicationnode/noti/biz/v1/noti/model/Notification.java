package com.sparwk.communicationnode.noti.biz.v1.noti.model;

import lombok.*;

@Getter@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class Notification {
    private String profileId;
    private String notiCode;
    private String notiMsg;
    private int viewCnt;
}
