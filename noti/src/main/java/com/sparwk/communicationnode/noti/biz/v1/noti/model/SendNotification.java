package com.sparwk.communicationnode.noti.biz.v1.noti.model;

import lombok.Builder;
import lombok.Data;
import lombok.ToString;

import java.util.Map;

@Data
@Builder
@ToString
public class SendNotification {
    private String profileId;
    private String notiCode;
    private String regUTCTime;
    private String notiImg;
    private String notiOwner;
    private boolean isView;
    private Map<String, String> data;
}
