var stompClient = null;

function setConnected(connected) {
    $("#connect").prop("disabled", connected);
    $("#disconnect").prop("disabled", !connected);
    if (connected) {
        $("#conversation").show();
    }
    else {
        $("#conversation").hide();
    }
    $("#greetings").html("");
}

function connect(username){ //1-1
    return new Promise((resolve, reject) => {
        let stompClient = Stomp.over(new SockJS('/notification'))
        stompClient.connect({}, (frame) => resolve(stompClient))
    })
}


function disconnect() {
    if (stompClient !== null) {
        stompClient.disconnect();
    }
    setConnected(false);
    console.log("Disconnected");
}

function sendName() {
    // /app/hello로 JSON 파라미터를 메세지 body로 전송.
    stompClient.send("/app/hello", {}, JSON.stringify({'name': $("#name").val()}));
}

function showGreeting(message) {
    $("#greetings").append("<tr><td>" + message + "</td></tr>");
}

function stompSubscribe(stompClient, endpoint, callback){ //8
    stompClient.subscribe(endpoint, callback)
    return stompClient
}

function stompClientSendMessage(stompClient, endpoint, message){ // 9
    stompClient.send(endpoint, {}, message)
    return stompClient
}

$(function () {
    let chatUsersList = [];
    $("form").on('submit', function (e) {
        e.preventDefault();
    });
    $( "#connect" ).click(function() {
        let username = $("#name").val();
        if(username == null || username === ''){
            alert('Name cannot be empty!!!')
        } else {
            connect(username) //1
                .then((stompClient) => stompSubscribe(stompClient, '/user/queue/newMember', (data) => { //2
                    chatUsersList = JSON.parse(data.body)
                    // if(chatUsersList.length > 0){
                    //     displayUserList(chatUsersList.filter(x => x != username), chatList, username, stompClient)
                    // } else {
                    //     alertMessage("Username already exists!!!", "bg-danger")
                    //     disconnect(stompClient, username, connectButton, disconnectButton)
                    // }
                    console.log(chatUsersList);
                })).then((stompClient) => stompSubscribe(stompClient, '/topic/newMember', (data) => {  // 3
                    chatUsersList.push(data.body);
                    //displayUserList(chatUsersList.filter(x => x != username), chatList, username, stompClient)
                    console.log(chatUsersList);
                }))
                .then((stompClient) => stompClientSendMessage(stompClient, '/app/register', username)) // 4
                .then((stompClient) => stompSubscribe(stompClient, `/user/${username}/msg`, (data) => {
                    //displayMessage(chatList, stompClient, username, JSON.parse(data.body))
                    $("#greetings").append("<tr><td>" + JSON.parse(data.body).profileId +" : "+ JSON.parse(data.body).notiMsg + "</td></tr>");

                }))
                // .then((stompClient) => { //5
                //     connectButton.disabled = true;
                //     disconnectButton.disabled = false;
                //     disconnectButton.addEventListener('click', () => disconnect(stompClient, username, connectButton, disconnectButton, true), true); // 6
                //     return stompClient;
                // })
                .then((stompClient) => stompSubscribe(stompClient, '/topic/disconnectedUser', (data) => { // 7
                const userWhoLeft = data.body;
                chatUsersList = chatUsersList.filter(x => x != userWhoLeft);
                // displayUserList(chatUsersList.filter(x => x != username), chatList, username, stompClient);
                alert(`User [${userWhoLeft}] left the chat room!!!`, "bg-success")
            }))
        }
    });
    $( "#disconnect" ).click(function() { disconnect(); });
    $( "#send" ).click(function() { sendName(); });


});