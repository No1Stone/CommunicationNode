package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyRoster is a Querydsl query type for ProfileCompanyRoster
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyRoster extends EntityPathBase<ProfileCompanyRoster> {

    private static final long serialVersionUID = 1530216542L;

    public static final QProfileCompanyRoster profileCompanyRoster = new QProfileCompanyRoster("profileCompanyRoster");

    public final StringPath joiningEndDt = createString("joiningEndDt");

    public final StringPath joiningEndYn = createString("joiningEndYn");

    public final StringPath joiningStartDt = createString("joiningStartDt");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePositionSeq = createNumber("profilePositionSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> rosterSeq = createNumber("rosterSeq", Long.class);

    public final StringPath rosterStatus = createString("rosterStatus");

    public QProfileCompanyRoster(String variable) {
        super(ProfileCompanyRoster.class, forVariable(variable));
    }

    public QProfileCompanyRoster(Path<? extends ProfileCompanyRoster> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyRoster(PathMetadata metadata) {
        super(ProfileCompanyRoster.class, metadata);
    }

}

