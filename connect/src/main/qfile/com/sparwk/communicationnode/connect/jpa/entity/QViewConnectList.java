package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QViewConnectList is a Querydsl query type for ViewConnectList
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewConnectList extends EntityPathBase<ViewConnectList> {

    private static final long serialVersionUID = 974826308L;

    private static final PathInits INITS = PathInits.DIRECT2;

    public static final QViewConnectList viewConnectList = new QViewConnectList("viewConnectList");

    public final StringPath city = createString("city");

    public final StringPath email = createString("email");

    public final StringPath locationCd = createString("locationCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final StringPath phoneNumber = createString("phoneNumber");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImg = createString("profileImg");

    public final StringPath profileName = createString("profileName");

    public final StringPath profileType = createString("profileType");

    public final QViewCompanyInfo ViewCompanyInfo;

    public final QViewGroupInfo ViewGroupInfo;

    public final QViewUserInfo viewUserInfo;

    public final StringPath website = createString("website");

    public QViewConnectList(String variable) {
        this(ViewConnectList.class, forVariable(variable), INITS);
    }

    public QViewConnectList(Path<? extends ViewConnectList> path) {
        this(path.getType(), path.getMetadata(), PathInits.getFor(path.getMetadata(), INITS));
    }

    public QViewConnectList(PathMetadata metadata) {
        this(metadata, PathInits.getFor(metadata, INITS));
    }

    public QViewConnectList(PathMetadata metadata, PathInits inits) {
        this(ViewConnectList.class, metadata, inits);
    }

    public QViewConnectList(Class<? extends ViewConnectList> type, PathMetadata metadata, PathInits inits) {
        super(type, metadata, inits);
        this.ViewCompanyInfo = inits.isInitialized("ViewCompanyInfo") ? new QViewCompanyInfo(forProperty("ViewCompanyInfo")) : null;
        this.ViewGroupInfo = inits.isInitialized("ViewGroupInfo") ? new QViewGroupInfo(forProperty("ViewGroupInfo")) : null;
        this.viewUserInfo = inits.isInitialized("viewUserInfo") ? new QViewUserInfo(forProperty("viewUserInfo")) : null;
    }

}

