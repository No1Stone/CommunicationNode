package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileInterestMeta is a Querydsl query type for ProfileInterestMeta
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileInterestMeta extends EntityPathBase<ProfileInterestMeta> {

    private static final long serialVersionUID = -600712551L;

    public static final QProfileInterestMeta profileInterestMeta = new QProfileInterestMeta("profileInterestMeta");

    public final StringPath detailTypeCd = createString("detailTypeCd");

    public final StringPath kindTypeCd = createString("kindTypeCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public QProfileInterestMeta(String variable) {
        super(ProfileInterestMeta.class, forVariable(variable));
    }

    public QProfileInterestMeta(Path<? extends ProfileInterestMeta> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileInterestMeta(PathMetadata metadata) {
        super(ProfileInterestMeta.class, metadata);
    }

}

