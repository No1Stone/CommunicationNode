package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileConnect is a Querydsl query type for ProfileConnect
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileConnect extends EntityPathBase<ProfileConnect> {

    private static final long serialVersionUID = 80864352L;

    public static final QProfileConnect profileConnect = new QProfileConnect("profileConnect");

    public final StringPath bookmarkYn = createString("bookmarkYn");

    public final NumberPath<Long> connectNum = createNumber("connectNum", Long.class);

    public final NumberPath<Long> connectProfileId = createNumber("connectProfileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileType = createString("profileType");

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QProfileConnect(String variable) {
        super(ProfileConnect.class, forVariable(variable));
    }

    public QProfileConnect(Path<? extends ProfileConnect> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileConnect(PathMetadata metadata) {
        super(ProfileConnect.class, metadata);
    }

}

