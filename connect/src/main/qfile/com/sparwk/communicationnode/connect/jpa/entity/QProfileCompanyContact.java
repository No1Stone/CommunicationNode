package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileCompanyContact is a Querydsl query type for ProfileCompanyContact
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileCompanyContact extends EntityPathBase<ProfileCompanyContact> {

    private static final long serialVersionUID = -240202579L;

    public static final QProfileCompanyContact profileCompanyContact = new QProfileCompanyContact("profileCompanyContact");

    public final StringPath contctEmail = createString("contctEmail");

    public final StringPath contctFirstName = createString("contctFirstName");

    public final StringPath contctLastName = createString("contctLastName");

    public final StringPath contctMidleName = createString("contctMidleName");

    public final StringPath contctPhoneNumber = createString("contctPhoneNumber");

    public final StringPath countryCd = createString("countryCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final StringPath profileContactDescription = createString("profileContactDescription");

    public final StringPath profileContactImgUrl = createString("profileContactImgUrl");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath verifyPhoneYn = createString("verifyPhoneYn");

    public QProfileCompanyContact(String variable) {
        super(ProfileCompanyContact.class, forVariable(variable));
    }

    public QProfileCompanyContact(Path<? extends ProfileCompanyContact> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileCompanyContact(PathMetadata metadata) {
        super(ProfileCompanyContact.class, metadata);
    }

}

