package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfileAnrPitching is a Querydsl query type for ProfileAnrPitching
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfileAnrPitching extends EntityPathBase<ProfileAnrPitching> {

    private static final long serialVersionUID = -1124010467L;

    public static final QProfileAnrPitching profileAnrPitching = new QProfileAnrPitching("profileAnrPitching");

    public final StringPath pitchingYn = createString("pitchingYn");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public QProfileAnrPitching(String variable) {
        super(ProfileAnrPitching.class, forVariable(variable));
    }

    public QProfileAnrPitching(Path<? extends ProfileAnrPitching> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfileAnrPitching(PathMetadata metadata) {
        super(ProfileAnrPitching.class, metadata);
    }

}

