package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QViewGroupInfo is a Querydsl query type for ViewGroupInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewGroupInfo extends EntityPathBase<ViewGroupInfo> {

    private static final long serialVersionUID = -286326423L;

    public static final QViewGroupInfo viewGroupInfo = new QViewGroupInfo("viewGroupInfo");

    public final StringPath accntEmail = createString("accntEmail");

    public final StringPath city = createString("city");

    public final StringPath headline = createString("headline");

    public final StringPath locationCd = createString("locationCd");

    public final StringPath mattermostId = createString("mattermostId");

    public final StringPath permisionType = createString("permisionType");

    public final StringPath profileGroupName = createString("profileGroupName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final StringPath profileVerifyYn = createString("profileVerifyYn");

    public final StringPath region = createString("region");

    public final StringPath webSite = createString("webSite");

    public QViewGroupInfo(String variable) {
        super(ViewGroupInfo.class, forVariable(variable));
    }

    public QViewGroupInfo(Path<? extends ViewGroupInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QViewGroupInfo(PathMetadata metadata) {
        super(ViewGroupInfo.class, metadata);
    }

}

