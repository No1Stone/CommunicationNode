package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QAccountCompanyLocation is a Querydsl query type for AccountCompanyLocation
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QAccountCompanyLocation extends EntityPathBase<AccountCompanyLocation> {

    private static final long serialVersionUID = -1482788924L;

    public static final QAccountCompanyLocation accountCompanyLocation = new QAccountCompanyLocation("accountCompanyLocation");

    public final NumberPath<Long> accntId = createNumber("accntId", Long.class);

    public final StringPath locationCd = createString("locationCd");

    public QAccountCompanyLocation(String variable) {
        super(AccountCompanyLocation.class, forVariable(variable));
    }

    public QAccountCompanyLocation(Path<? extends AccountCompanyLocation> path) {
        super(path.getType(), path.getMetadata());
    }

    public QAccountCompanyLocation(PathMetadata metadata) {
        super(AccountCompanyLocation.class, metadata);
    }

}

