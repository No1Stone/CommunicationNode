package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QViewCompanyInfo is a Querydsl query type for ViewCompanyInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewCompanyInfo extends EntityPathBase<ViewCompanyInfo> {

    private static final long serialVersionUID = -1257627289L;

    public static final QViewCompanyInfo viewCompanyInfo = new QViewCompanyInfo("viewCompanyInfo");

    public final StringPath accntEmail = createString("accntEmail");

    public final StringPath city = createString("city");

    public final StringPath headline = createString("headline");

    public final StringPath locationCd = createString("locationCd");

    public final StringPath mattermostId = createString("mattermostId");

    public final StringPath permisionType = createString("permisionType");

    public final StringPath profileCompanyName = createString("profileCompanyName");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final StringPath profileVerifyYn = createString("profileVerifyYn");

    public final StringPath region = createString("region");

    public final StringPath webSite = createString("webSite");

    public QViewCompanyInfo(String variable) {
        super(ViewCompanyInfo.class, forVariable(variable));
    }

    public QViewCompanyInfo(Path<? extends ViewCompanyInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QViewCompanyInfo(PathMetadata metadata) {
        super(ViewCompanyInfo.class, metadata);
    }

}

