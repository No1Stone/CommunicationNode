package com.sparwk.communicationnode.connect.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QViewUserInfo is a Querydsl query type for ViewUserInfo
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QViewUserInfo extends EntityPathBase<ViewUserInfo> {

    private static final long serialVersionUID = 1157282301L;

    public static final QViewUserInfo viewUserInfo = new QViewUserInfo("viewUserInfo");

    public final StringPath accntEmail = createString("accntEmail");

    public final StringPath bio = createString("bio");

    public final StringPath currentCityCountryCd = createString("currentCityCountryCd");

    public final StringPath currentCityNm = createString("currentCityNm");

    public final StringPath fullName = createString("fullName");

    public final StringPath headline = createString("headline");

    public final StringPath language = createString("language");

    public final StringPath mattermostId = createString("mattermostId");

    public final StringPath permisionType = createString("permisionType");

    public final StringPath phoneNumber = createString("phoneNumber");

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final StringPath profileImgUrl = createString("profileImgUrl");

    public final ListPath<ProfileInterestMeta, QProfileInterestMeta> profileInterestMeta = this.<ProfileInterestMeta, QProfileInterestMeta>createList("profileInterestMeta", ProfileInterestMeta.class, QProfileInterestMeta.class, PathInits.DIRECT2);

    public final StringPath profileVerifyYn = createString("profileVerifyYn");

    public QViewUserInfo(String variable) {
        super(ViewUserInfo.class, forVariable(variable));
    }

    public QViewUserInfo(Path<? extends ViewUserInfo> path) {
        super(path.getType(), path.getMetadata());
    }

    public QViewUserInfo(PathMetadata metadata) {
        super(ViewUserInfo.class, metadata);
    }

}

