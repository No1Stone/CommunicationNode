package com.sparwk.communicationnode.connect.biz.v1;

import com.google.gson.Gson;
import com.sparwk.communicationnode.connect.biz.v1.dto.*;
import com.sparwk.communicationnode.connect.biz.v1.mattermost.MmCompany;
import com.sparwk.communicationnode.connect.config.filter.responsepack.Response;
import com.sparwk.communicationnode.connect.jpa.repository.ViewConnectListRepository;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.RequiredArgsConstructor;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/connect")
@CrossOrigin("*")
@Api(tags = "Profile Server Connect API")
@RequiredArgsConstructor
public class ProfileConnectController {

    @Autowired
    private ProfileConnectService profileConnectService;
    private final ViewConnectListRepository viewConnectListRepository;
    private final Logger logger = LoggerFactory.getLogger(ProfileConnectController.class);

    @ApiOperation(
            value = "컨넥트 정보입력",
            notes = "컨넥트 정보입력"
    )
    @PostMapping(path = "/info")
    public Response ProfileConnectSaveController(@Valid @RequestBody ProfileConnectRequest dto) {
        Response res = profileConnectService.ProfileConnectSaveService(dto);
        return res;
    }

//안씀
//    @ApiOperation(
//            value = "컨넥트 리스트 정보입력",
//            notes = "컨넥트 리스트 정보입력"
//    )
//    @PostMapping(path = "/info/list")
//    public Response ProfileConnectListSaveController(@Valid @RequestBody ProfileConnectRequestList dto) {
//        Response res = profileConnectService.ProfileConnectListSaveService(dto);
//        return res;
//    }

    @PostMapping(path = "/info/detail/{profileid}")
    public Response ProfileConnectListdetailSelectController(@RequestBody ViewProfileRequest dto) {
        Response res = profileConnectService.ProfileConnectListdetailSelectService(dto);
        return res;
    }

    @PostMapping(path = "/info/list/filter/{profileId}")
    public Response ProfileConnectListFilterSelectController(@RequestBody ProfileConnectAddListRequest dto,
    @PathVariable(name = "profileId")Long profileId) {
        Response res = profileConnectService.ProfileConnectListFilterSelectService(dto, profileId);
        return res;
    }

    @GetMapping(path = "/info/{profileid}")
    public Response ProfileConnectListSelectController(@PathVariable(name = "profileid") Long profileid) {
        Response res = profileConnectService.ProfileConnectSelectService(profileid);
        return res;
    }

    @PostMapping(path = "/info/delete")
    public Response ProfileConnectDeleteController(@Valid @RequestBody ProfileConnectDeleteRequest dto) {
        Response res = profileConnectService.ProfileConnectdeleteService(dto);
        return res;
    }

    @PostMapping(path = "/info/bookmark")
    public Response ProfileConnectBoolmarkUpdateController(@RequestBody ProfileConnectBookmarkRequest dto) {
        Response res = profileConnectService.ProfileConnectBoolmarkUpdateService(dto);
        return res;
    }

    @PostMapping(path = "/addlist")
    public Response ProfileConnectAddListController(@RequestBody ProfileConnectAddListRequest dto) {
        Response res = profileConnectService.ProfileConnectAddListService(dto);
        return res;
    }

//    @ApiOperation(
//            value = "Connect Add List 필터적용",
//            notes = "필터에따라 In query로 검색합니다."
//    )
//    @PostMapping(path = "/addlist/fliter")
//    public Response ProfileConnectAddListFilterController(@RequestBody ProfileConnectAddListRequest dto){
//        Response res = profileConnectService.ProfileConnectAddListFilterService(dto);
//        return res;
//    }

    @ApiOperation(
            value = "유저 : artist/ creator/ A&R 검색용",
            notes = "필터에따라 In query로 검색합니다."
    )
    @PostMapping(path = "/user/fliter")
    public Response ProfileConnectUserFilterController(@RequestBody ProfileConnectUserRequest dto) {
        Response res = profileConnectService.ProfileConnectUserFilterService(dto);
        return res;
    }

    @ApiOperation(
            value = "Connect List search",
            notes = "In query로 검색합니다."
    )
    @PostMapping(path = "/view/connectlist/in")
    public Response ProfileConnectViewConnectListInController(@RequestBody ViewProfileListRequest dto) {
        Response res = profileConnectService.ProfileConnectViewConnectListInService(dto);
        return res;
    }

    @ApiOperation(
            value = "User List search",
            notes = "In query로 검색합니다."
    )
    @PostMapping(path = "/view/user/in")
    public Response ProfileConnectViewUserListInController(@RequestBody ViewProfileListRequest dto) {
        Response res = profileConnectService.ProfileConnectViewUserListInService(dto);
        return res;
    }

    @ApiOperation(
            value = "Company List search",
            notes = "In query로 검색합니다."
    )
    @PostMapping(path = "/view/company/in")
    public Response ProfileConnectViewCompanyListInController(@RequestBody ViewProfileListRequest dto) {
        Response res = profileConnectService.ProfileConnectViewCompanyListInService(dto);
        return res;
    }

    @ApiOperation(
            value = "Group List search",
            notes = "In query로 검색합니다."
    )
    @PostMapping(path = "/view/group/in")
    public Response ProfileConnectViewGroupListInController(@RequestBody ViewProfileListRequest dto) {
        Response res = profileConnectService.ProfileConnectViewGroupListInService(dto);
        return res;
    }

    @ApiOperation(
            value = "User search",
            notes = "profile Id 단일 검색."
    )
    @GetMapping(path = "/view/user/one/{profileId}")
    public Response ProfileConnectViewUserOneController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileConnectService.ProfileConnectViewUserOneService(profileId);
        return res;
    }
    @ApiOperation(
            value = "Company search",
            notes = "profile Id 단일 검색."
    )
    @GetMapping(path = "/view/company/one/{profileId}")
    public Response ProfileConnectViewCompanyOneController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileConnectService.ProfileConnectViewCompanyOneService(profileId);
        return res;
    }
    @ApiOperation(
            value = "Group search",
            notes = "profile Id 단일 검색."
    )
    @GetMapping(path = "/view/group/one/{profileId}")
    public Response ProfileConnectViewGroupOneController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profileConnectService.ProfileConnectViewGroupOneService(profileId);
        return res;
    }

    @PostMapping(path = "/company/partner/anr/list")
    public Response ProfileConnectCompanyPartnerAnrListController(@RequestBody ViewProfileRequest dto) {
        Response res = profileConnectService.ProfileConnectCompanyPartnerAnrListServuce(dto);
        return res;
    }

    @RequestMapping(value = "/requestNotification", method = RequestMethod.POST)
    public void requestNotification(@RequestBody ReceiveNotification receiveNotification) {
    }

    @GetMapping(path = "/test111/{profileId}")
    public void TestRequest(@PathVariable(name = "profileId")Long profileId) {
        MmCompany mc = new MmCompany();
        mc.setCompanyId(profileId);
        mc.setCompanySparwkId(profileId);
        logger.info("팀아이디 - {}", mc.getTeamid(mc));

    }
}
