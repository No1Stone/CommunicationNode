package com.sparwk.communicationnode.connect.jpa.entity;

import com.sparwk.communicationnode.connect.jpa.entity.id.ProfileCompanyPartnerId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company_partner")
@DynamicUpdate
@IdClass(ProfileCompanyPartnerId.class)
public class ProfileCompanyPartner {

    @Id
    @Column(name = "partner_id", nullable = true)
    private Long partnerId;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "relation_type", nullable = true)
    private String relationType;
    @Column(name = "relation_status", nullable = true)
    private String relationStatus;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileCompanyPartner(
            Long profileId,
            Long partnerId,
            String relationType,
            String relationStatus,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
            ){
        this.profileId=profileId;
        this.partnerId=partnerId;
        this.relationType=relationType;
        this.relationStatus=relationStatus;
        this.regUsr=regUsr;
        this.regDt=regDt;
        this.modUsr=modUsr;
        this.modDt=modDt;
    }


}
