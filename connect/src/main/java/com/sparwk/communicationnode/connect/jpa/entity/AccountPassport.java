package com.sparwk.communicationnode.connect.jpa.entity;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_passport")
@DynamicUpdate
public class AccountPassport {
    @Id
    @Column(name = "accnt_id")
    private Long accntId;

    @Column(name = "passport_first_name", nullable = true, length = 50)
    private String passportFirstName;

    @Column(name = "passport_middle_name", nullable = true, length = 50)
    private String passportMiddleName;

    @Column(name = "passport_last_name", nullable = true, length = 50)
    private String passportLastName;

    @Column(name = "passport_img_file_url", nullable = true, length = 200)
    private String passportImgFileUrl;

    @Column(name = "personal_info_collection_yn", nullable = true, length = 1)
    private String personalInfoCollectionYn;

    @Column(name = "country_cd", nullable = true, length = 9)
    private String countryCd;

    @Column(name = "passport_verify_yn", nullable = true, length = 9)
    private String passportVerifyYn;

    @Builder
    AccountPassport(
            Long accntId,
            String passportFirstName,
            String passportMiddleName,
            String passportLastName,
            String passportImgFileUrl,
            String personalInfoCollectionYn,
            String countryCd,
            String passportVerifyYn
    ) {
        this.accntId = accntId;
        this.passportFirstName = passportFirstName;
        this.passportMiddleName = passportMiddleName;
        this.passportLastName = passportLastName;
        this.passportImgFileUrl = passportImgFileUrl;
        this.personalInfoCollectionYn = personalInfoCollectionYn;
        this.countryCd = countryCd;
        this.passportVerifyYn = passportVerifyYn;
    }
}
