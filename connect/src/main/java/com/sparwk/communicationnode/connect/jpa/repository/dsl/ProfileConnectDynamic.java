package com.sparwk.communicationnode.connect.jpa.repository.dsl;

import com.sparwk.communicationnode.connect.jpa.dto.ProfileConnectBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.ProfileConnect;
import org.springframework.transaction.annotation.Transactional;

public interface ProfileConnectDynamic {
    @Transactional
    Long ProfileConnectDynamicUpdate(ProfileConnectBaseDTO dto);
}
