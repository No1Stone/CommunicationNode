package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileGroup;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileGroupRepository extends JpaRepository<ProfileGroup, Long> {
    List<ProfileGroup> findByAccntIdIn(List<Long> profileConvert);
}
