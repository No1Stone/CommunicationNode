package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileInterestMeta;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileInterestMetaRepository extends JpaRepository<ProfileInterestMeta, Long> {

    List<ProfileInterestMeta> findByDetailTypeCdInAndProfileIdIn(List<String> cd, List<Long> profileId);
    List<ProfileInterestMeta> findByProfileId(Long profileId);

}
