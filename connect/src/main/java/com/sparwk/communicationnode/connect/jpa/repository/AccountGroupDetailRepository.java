package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.AccountGroupDetail;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountGroupDetailRepository extends JpaRepository<AccountGroupDetail, Long> {

    List<AccountGroupDetail> findByLocationCdIn(List<String> cd);

}
