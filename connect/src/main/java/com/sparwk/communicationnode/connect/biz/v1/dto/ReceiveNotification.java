package com.sparwk.communicationnode.connect.biz.v1.dto;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class ReceiveNotification {
    private String notiCode;
    private List<String> toUsers;
    private String notiOwner;
    private Map<String, String> data;
}
