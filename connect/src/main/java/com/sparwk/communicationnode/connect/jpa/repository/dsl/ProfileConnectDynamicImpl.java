package com.sparwk.communicationnode.connect.jpa.repository.dsl;

import com.querydsl.jpa.impl.JPAQueryFactory;
import com.querydsl.jpa.impl.JPAUpdateClause;
import com.sparwk.communicationnode.connect.jpa.dto.ProfileConnectBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.QProfileConnect;
import org.aspectj.lang.annotation.Aspect;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;

@Aspect
public class ProfileConnectDynamicImpl implements ProfileConnectDynamic {

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private final Logger logger = LoggerFactory.getLogger(ProfileConnectDynamicImpl.class);
    private QProfileConnect qProfileConnect = QProfileConnect.profileConnect;

    private ProfileConnectDynamicImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }

    @Override
    public Long ProfileConnectDynamicUpdate(ProfileConnectBaseDTO entity) {
        JPAUpdateClause jpaUpdateClause = new JPAUpdateClause(em, qProfileConnect);

        if (entity.getConnectNum() != null) {
            jpaUpdateClause.set(qProfileConnect.connectNum, entity.getConnectNum());
        }
        if (entity.getProfileType() == null || entity.getProfileType().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileConnect.profileType, entity.getProfileType());
        }
        if (entity.getBookmarkYn() == null || entity.getBookmarkYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileConnect.bookmarkYn, entity.getBookmarkYn());
        }
        if (entity.getUseYn() == null || entity.getUseYn().isEmpty()) {
        } else {
            jpaUpdateClause.set(qProfileConnect.useYn, entity.getUseYn());
        }
        if (entity.getRegUsr() != null) {
            jpaUpdateClause.set(qProfileConnect.regUsr, entity.getRegUsr());
        }
        if (entity.getRegDt() != null) {
            jpaUpdateClause.set(qProfileConnect.regDt, entity.getRegDt());
        }
        if (entity.getModUsr() != null) {
            jpaUpdateClause.set(qProfileConnect.modUsr, entity.getModUsr());
        }
        if (entity.getModDt() != null) {
            jpaUpdateClause.set(qProfileConnect.modDt, entity.getModDt());
        }

            jpaUpdateClause.where(qProfileConnect.profileId.eq(entity.getProfileId()));
            jpaUpdateClause.where(qProfileConnect.connectProfileId.eq(entity.getConnectProfileId()));


        long result = jpaUpdateClause.execute();
        em.flush();
        em.clear();
        return result;
    }

}
