package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileCompanyRoster;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyRosterRepository extends JpaRepository<ProfileCompanyRoster, Long> {

    List<ProfileCompanyRoster> findByProfileIdInAndRosterStatus(List<Long>profileId, String status);
}
