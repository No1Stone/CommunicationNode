package com.sparwk.communicationnode.connect.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_company")
@DynamicUpdate
public class ProfileCompany {

    @Id
    @GeneratedValue(generator = "profile_id_seq")
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "profile_company_name", nullable = true)
    private String profileCompanyName;
    @Column(name = "mattermost_id", nullable = true)
    private String mattermostId;
    @Column(name = "profile_img_url", nullable = true)
    private String profileImgUrl;
    @Column(name = "profile_bgd_img_url", nullable = true)
    private String profileBgdImgUrl;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "bio", nullable = true)
    private String bio;
    @Column(name = "com_info_overview", nullable = true)
    private String comInfoOverview;
    @Column(name = "com_info_website", nullable = true)
    private String comInfoWebsite;
    @Column(name = "com_info_phone", nullable = true)
    private String comInfoPhone;
    @Column(name = "com_info_email", nullable = true)
    private String comInfoEmail;
    @Column(name = "com_info_found", nullable = true)
    private String comInfoFound;
    @Column(name = "com_info_country_cd", nullable = true)
    private String comInfoCountryCd;
    @Column(name = "com_info_address", nullable = true)
    private String comInfoAddress;
    @Column(name = "com_info_lat", nullable = true)
    private String comInfoLat;
    @Column(name = "com_info_lon", nullable = true)
    private String comInfoLon;
    @Column(name = "ipi_number", nullable = true)
    private String ipiNumber;
    @Column(name = "ipi_number_varify_yn", nullable = true)
    private String ipiNumberVarifyYn;
    @Column(name = "vat_number", nullable = true)
    private String vatNumber;
    @Column(name = "vat_number_varify_yn", nullable = true)
    private String vatNumberVarifyYn;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Column(name = "com_info_location", nullable = true)
    private String comInfoLocation;


    @Builder
    ProfileCompany(
            Long profileId,
            Long accntId,
            String mattermostId,
            String profileImgUrl,
            String profileBgdImgUrl,
            String profileCompanyName,
            String headline,
            String bio,
            String comInfoOverview,
            String comInfoWebsite,
            String comInfoPhone,
            String comInfoEmail,
            String comInfoFound,
            String comInfoCountryCd,
            String comInfoAddress,
            String comInfoLat,
            String comInfoLon,
            String ipiNumber,
            String ipiNumberVarifyYn,
            String vatNumber,
            String vatNumberVarifyYn,
            String comInfoLocation,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.profileCompanyName = profileCompanyName;
        this.mattermostId = mattermostId;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
        this.headline = headline;
        this.bio = bio;
        this.comInfoOverview = comInfoOverview;
        this.comInfoWebsite = comInfoWebsite;
        this.comInfoPhone = comInfoPhone;
        this.comInfoEmail = comInfoEmail;
        this.comInfoFound = comInfoFound;
        this.comInfoCountryCd = comInfoCountryCd;
        this.comInfoAddress = comInfoAddress;
        this.comInfoLat = comInfoLat;
        this.comInfoLon = comInfoLon;
        this.ipiNumber = ipiNumber;
        this.ipiNumberVarifyYn = ipiNumberVarifyYn;
        this.vatNumber = vatNumber;
        this.vatNumberVarifyYn = vatNumberVarifyYn;
        this.comInfoLocation = comInfoLocation;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }

}
