package com.sparwk.communicationnode.connect.jpa.repository.dsl;

import com.sparwk.communicationnode.connect.jpa.dto.ViewConnectListBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.ViewConnectList;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewConnectListDsl  {

    List<ViewConnectListBaseDTO> ListDynamicSelect(String name , List<String> location);

}
