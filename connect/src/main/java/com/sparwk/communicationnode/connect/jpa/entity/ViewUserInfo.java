package com.sparwk.communicationnode.connect.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_user_info")
@DynamicUpdate
public class ViewUserInfo {

    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "full_name", nullable = true)
    private String fullName;
    @Column(name = "bio", nullable = true)
    private String bio;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "current_city_country_cd", nullable = true)
    private String currentCityCountryCd;
    @Column(name = "current_city_nm", nullable = true)
    private String currentCityNm;
    @Column(name = "profile_img_url", nullable = true)
    private String profileImgUrl;
    @Column(name = "phone_number", nullable = true)
    private String phoneNumber;
    @Column(name = "language", nullable = true)
    private String language;
    @Column(name = "mattermost_id", nullable = true)
    private String mattermostId;
    @Column(name = "accnt_email", nullable = true)
    private String accntEmail;
    @Column(name = "permision_type", nullable = true)
    private String permisionType;
    @Column(name = "profile_verify_yn", nullable = true)
    private String profileVerifyYn;


    @Builder
    ViewUserInfo(Long profileId,
                 String fullName,
                 String bio,
                 String headline,
                 String currentCityCountryCd,
                 String currentCityNm,
                 String profileImgUrl,
                 String phoneNumber,
                 String language,
                 String mattermostId,
                 String accntEmail,
                 String permisionType,
                 String profileVerifyYn
    ) {
        this.profileId = profileId;
        this.fullName = fullName;
        this.bio = bio;
        this.headline = headline;
        this.currentCityCountryCd = currentCityCountryCd;
        this.currentCityNm = currentCityNm;
        this.profileImgUrl = profileImgUrl;
        this.phoneNumber = phoneNumber;
        this.language = language;
        this.mattermostId = mattermostId;
        this.accntEmail = accntEmail;
        this.permisionType = permisionType;
        this.profileVerifyYn = profileVerifyYn;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfileInterestMeta.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private List<ProfileInterestMeta> profileInterestMeta;

    /*

CREATE OR REPLACE VIEW sparwk01.view_user_info
 AS
 SELECT pp.profile_id,
    pp.full_name,
    pp.bio,
    pp.headline,
    pp.current_city_country_cd,
    pp.current_city_nm,
    pp.profile_img_url,
    aa.phone_number,
    ( SELECT string_agg(pl.language_cd::text, ','::text) AS string_agg
           FROM tb_profile_language pl
          WHERE pl.profile_id = pp.profile_id) AS language,
    pp.mattermost_id,
    aa.accnt_email,
    ( SELECT (((
                CASE
                    WHEN (EXISTS ( SELECT tpp_1.artist_yn
                       FROM tb_profile_position tpp_1
                      WHERE tpp_1.profile_id = pp.profile_id AND tpp_1.artist_yn = 'Y'::bpchar)) THEN 'artist'::character varying::text
                    ELSE ''::text
                END || ','::text) ||
                CASE
                    WHEN (EXISTS ( SELECT tpp_1.anr_yn
                       FROM tb_profile_position tpp_1
                      WHERE tpp_1.profile_id = pp.profile_id AND tpp_1.anr_yn = 'Y'::bpchar)) THEN 'anr'::character varying::text
                    ELSE ''::text
                END) || ','::text) ||
                CASE
                    WHEN (EXISTS ( SELECT tpp_1.creator_yn
                       FROM tb_profile_position tpp_1
                      WHERE tpp_1.profile_id = pp.profile_id AND tpp_1.creator_yn = 'Y'::bpchar)) THEN 'creator'::character varying::text
                    ELSE ''::text
                END) AS permision_type,
    pp.verify_yn AS profile_verify_yn
   FROM tb_profile pp
     LEFT JOIN tb_account aa ON pp.accnt_id = aa.accnt_id;
     */

}
