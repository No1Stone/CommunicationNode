package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.AccountPassport;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountPassportRepository extends JpaRepository<AccountPassport, Long> {
    List<AccountPassport> findByCountryCdInAndAccntIdIn(List<String> cd, List<Long>accountId);
}
