package com.sparwk.communicationnode.connect.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectDeleteRequest {

    private Long profileId;
    private Long connectProfileId;

}
