package com.sparwk.communicationnode.connect.biz.v1.mattermost;

import com.google.gson.Gson;
import com.sparwk.communicationnode.connect.config.common.RestCall;
import com.sparwk.communicationnode.connect.config.filter.responsepack.Response;
import org.json.JSONObject;

public class MmCompany {

    protected String awsURL =    "https://personalization.sparwkdev.com/profile/V1/profile/public/mmGet/";
    protected String awscomURL = "https://personalization.sparwkdev.com/profile/V1/profile/public/mmComGet/";
//        protected String awsURL =     "http://localhost:8102/profile/V1/profile/public/mmGet/";
//        protected String awscomURL =  "http://localhost:8102/profile/V1/profile/public/mmComGet/";
    private String baseURL = "https://communication.sparwkdev.com/cowork/V1";

    //metterMost_id
    private String companyId;
    //Sparwk Id
    private String companySparwkId;
    //추가되는사람 metterMost_Id
    private String profileId;

    public String getBaseURL() {
        return baseURL;
    }
    public String getCompanyId() {
        return companyId;
    }


    public String getCompanySparwkId() {
        return companySparwkId;
    }

    public void setCompanySparwkId(Long profileId) {
        this.companySparwkId =
        RestCall.get(awscomURL+profileId).toString();
        System.out.println(this.companySparwkId);
    }
    public void setCompanySparwkId(String profileId) {
        this.companySparwkId = profileId;
    }

    public void setCompanyId(Long profileId) {
        String id = RestCall.get(awsURL+profileId).toString();
        this.companyId = id;
        System.out.println(this.companyId);
    }

    public String getProfileId() {
        return "";
    }

    public String getTeamid(MmCompany mc){
        if(mc.getCompanySparwkId().equals("0")) mc.setCompanySparwkId("");
        this.companySparwkId = mc.getCompanySparwkId();
        String result = new Gson().toJson(mc);
        String data = RestCall.postRes(baseURL+"/common/get/teamid",result);
        String jo = new JSONObject(data).getString("data");
        System.out.println("data result "+ jo);


        return jo;
    }

}
