package com.sparwk.communicationnode.connect.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyPartnerId implements Serializable {
    private Long partnerId;
    private Long profileId;
}
