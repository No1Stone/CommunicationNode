package com.sparwk.communicationnode.connect.biz.v1.dto;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileInterestMeta;
import com.sparwk.communicationnode.connect.jpa.entity.ViewCompanyInfo;
import com.sparwk.communicationnode.connect.jpa.entity.ViewGroupInfo;
import com.sparwk.communicationnode.connect.jpa.entity.ViewUserInfo;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewConnectListResponse {

    private String profileType;
    private Long profileId;
    private String profileImg;
    private String profileName;
    private String website;
    private String locationCd;
    private String city;
    private String email;
    private String phoneNumber;
    private LocalDateTime modDt;
    @Schema(description = "그룹멤버 또는 유저소속회사")
    private Object cooperation;
//    private ViewCompanyInfo viewCompanyInfo;
//    private ViewGroupInfo viewGroupInfo;
//    private ViewUserInfo viewUserInfo;
}
