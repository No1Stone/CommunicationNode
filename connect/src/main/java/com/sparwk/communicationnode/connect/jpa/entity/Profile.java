package com.sparwk.communicationnode.connect.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile")
@DynamicUpdate
public class Profile {

    @Id
    @GeneratedValue(generator = "profile_id_seq")
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "mattermost_id", nullable = true)
    private String mattermostId;
    @Column(name = "full_name", nullable = true)
    private String fullName;
    @Column(name = "stage_name_yn", nullable = true)
    private String stageNameYn;
    @Column(name = "bth_year", nullable = true)
    private String bthYear;
    @Column(name = "bth_month", nullable = true)
    private String bthMonth;
    @Column(name = "bth_day", nullable = true)
    private String bthDay;
    @Column(name = "hire_me_yn", nullable = true)
    private String hireMeYn;
    @Column(name = "bio", nullable = true)
    private String bio;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "current_city_country_cd", nullable = true)
    private String currentCityCountryCd;
    @Column(name = "current_city_nm", nullable = true)
    private String currentCityNm;
    @Column(name = "home_town_country_cd", nullable = true)
    private String homeTownCountryCd;
    @Column(name = "home_town_nm", nullable = true)
    private String homeTownNm;
    @Column(name = "ipi_info", nullable = true)
    private String ipiInfo;
    @Column(name = "cae_info", nullable = true)
    private String caeInfo;
    @Column(name = "isni_info", nullable = true)
    private String isniInfo;
    @Column(name = "ipn_info", nullable = true)
    private String ipnInfo;
    @Column(name = "nro_type_cd", nullable = true)
    private String nroTypeCd;
    @Column(name = "nro_info", nullable = true)
    private String nroInfo;
    @Column(name = "profile_img_url", nullable = true)
    private String profileImgUrl;
    @Column(name = "profile_bgd_img_url", nullable = true)
    private String profileBgdImgUrl;
    @Column(name = "verify_yn", nullable = true)
    private String verifyYn;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "accomplishments_info", nullable = true)
    private String accomplishmentsInfo;
    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    Profile(
            Long profileId,
            Long accntId,
            String mattermostId,
            String fullName,
            String stageNameYn,
            String bthYear,
            String bthMonth,
            String bthDay,
            String hireMeYn,
            String bio,
            String headline,
            String currentCityCountryCd,
            String currentCityNm,
            String homeTownCountryCd,
            String homeTownNm,
            String ipiInfo,
            String caeInfo,
            String isniInfo,
            String ipnInfo,
            String nroTypeCd,
            String nroInfo,
            String profileImgUrl,
            String profileBgdImgUrl,
            String verifyYn,
            String useYn,
            String accomplishmentsInfo,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.accntId = accntId;
        this.mattermostId = mattermostId;
        this.fullName = fullName;
        this.stageNameYn = stageNameYn;
        this.bthYear = bthYear;
        this.bthMonth = bthMonth;
        this.bthDay = bthDay;
        this.hireMeYn = hireMeYn;
        this.bio = bio;
        this.headline = headline;
        this.currentCityCountryCd = currentCityCountryCd;
        this.currentCityNm = currentCityNm;
        this.homeTownCountryCd = homeTownCountryCd;
        this.homeTownNm = homeTownNm;
        this.ipiInfo = ipiInfo;
        this.caeInfo = caeInfo;
        this.isniInfo = isniInfo;
        this.ipnInfo = ipnInfo;
        this.nroTypeCd = nroTypeCd;
        this.nroInfo = nroInfo;
        this.profileImgUrl = profileImgUrl;
        this.profileBgdImgUrl = profileBgdImgUrl;
        this.verifyYn = verifyYn;
        this.useYn = useYn;
        this.accomplishmentsInfo = accomplishmentsInfo;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;

    }
}
