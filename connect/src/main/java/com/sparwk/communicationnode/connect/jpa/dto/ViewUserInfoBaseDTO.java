package com.sparwk.communicationnode.connect.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewUserInfoBaseDTO {
    private Long profileId;
    private String fullName;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String profileImgUrl;
    private String phoneNumber;
    private String language;
    private String mattermostId;
    private String accntEmail;
    private String permisionType;
    private String profileVerifyYn;
}
