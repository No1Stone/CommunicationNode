package com.sparwk.communicationnode.connect.biz.v1.mattermost;

import com.google.gson.Gson;
import com.sparwk.communicationnode.connect.config.common.RestCall;
import lombok.Data;

public class MmFromTo {

    protected String awsURL = "https://personalization.sparwkdev.com/profile/V1/profile/public/mmGet/";
//    protected String awsURL = "http://localhost:8102/profile/V1/profile/public/mmGet/";
    private String baseURL = "https://communication.sparwkdev.com/cowork/V1";
    private String fromId;
    private String toId;

    public String getFromId() {
        return fromId;
    }

    public String getToId() {
        return toId;
    }

    public String getBaseURL() {
        return baseURL;
    }

    public void setFromId(Long fromId) {
        this.fromId = RestCall.get(awsURL+fromId).toString();
        System.out.println(fromId);
        System.out.println(this.fromId);
    }

    public void setToId(Long toId) {
        this.toId = RestCall.get(awsURL+toId).toString();
        System.out.println(toId);
        System.out.println(this.toId);
    }

    public void setFromId(String fromId) {
        this.fromId = fromId;
    }

    public void setToId(String toId) {
        this.toId = toId;
    }
}
