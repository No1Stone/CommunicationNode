package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.biz.v1.dto.ViewUserInfoFliterResponse;
import com.sparwk.communicationnode.connect.jpa.entity.ViewConnectList;
import com.sparwk.communicationnode.connect.jpa.repository.dsl.ViewConnectListDsl;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewConnectListRepository extends JpaRepository<ViewConnectList, Long>, ViewConnectListDsl {

    Page<ViewConnectList> findAllByLocationCdIn(List<String> location, Pageable pageable);

    Page<ViewConnectList> findByProfileNameLike(String name, Pageable pageable);

    Page<ViewConnectList> findByProfileNameLikeAndLocationCdIn(String name, List<String> location, Pageable pageable);

    Page<ViewConnectList> findByProfileIdIn(List<Long> profileList, Pageable pageable);

    List<ViewConnectList> findByProfileIdInAndProfileTypeOrderByModDtDesc(List<Long> profileList, String type);

    List<ViewConnectList> findByProfileIdInOrderByModDtDesc(List<Long> profileList);


    @Override
    Page<ViewConnectList> findAll(Pageable pageable);

    //    Page<ViewConnectList> findByProfileType(String type);
    List<ViewConnectList> findAllByOrderByModDtDesc();

    List<ViewConnectList> findByProfileType(String type);

    Page<ViewConnectList> findByProfileIdInOrderByModDtDesc(List<Long> profileId, Pageable pageable);
    List<ViewConnectList> findByProfileIdInOrderByModDtAsc(List<Long> profileId);
    List<ViewConnectList> findByProfileIdInAndProfileNameLikeOrderByModDtDesc(List<Long> profileId, String name);
    List<ViewConnectList> findByProfileIdInAndProfileNameLikeOrderByModDtAsc(List<Long> profileConnectId, String name);

}
