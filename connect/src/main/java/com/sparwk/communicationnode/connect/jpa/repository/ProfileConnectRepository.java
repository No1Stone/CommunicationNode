package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileConnect;
import com.sparwk.communicationnode.connect.jpa.repository.dsl.ProfileConnectDynamic;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

public interface ProfileConnectRepository extends JpaRepository<ProfileConnect, Long> , ProfileConnectDynamic {

    @Query("Select pc from ProfileConnect pc where pc.profileId = :profileId ORDER BY pc.bookmarkYn ASC, pc.regDt ASC ")
    List<ProfileConnect> doubleOrderby(Long profileId);

    List<ProfileConnect> findByProfileIdOrderByBookmarkYnAscRegDtDesc(Long profileId);
    List<ProfileConnect> findByProfileIdOrderByBookmarkYnDesc(Long profileId);
    List<ProfileConnect> findByProfileIdInOrderByBookmarkYnAscRegDtDesc(List<Long> profileId);
    Page<ProfileConnect> findByProfileIdAndConnectProfileIdInOrderByBookmarkYnDesc(Long profileId, List<Long> connectProfileId,  Pageable pageable);
    List<ProfileConnect> findByProfileIdOrderByModDt(Long profileId);

    @Transactional
    void deleteByProfileIdAndConnectProfileId(Long profileId, Long connectId);

    boolean existsByProfileIdAndConnectProfileId(Long profileId, Long connectId);
}
