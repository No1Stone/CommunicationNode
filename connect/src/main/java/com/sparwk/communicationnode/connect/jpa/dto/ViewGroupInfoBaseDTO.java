package com.sparwk.communicationnode.connect.jpa.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewGroupInfoBaseDTO {
    private Long profileId;
    private String profileGroupName;
    private String headline;
    private String locationCd;
    private String region;
    private String city;
    private String profileImgUrl;
    private String webSite;
    private String mattermostId;
    private String accntEmail;
    private String permisionType;
    private String profileVerifyYn;
}
