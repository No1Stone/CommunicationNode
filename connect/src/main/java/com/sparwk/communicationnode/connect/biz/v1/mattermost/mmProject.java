package com.sparwk.communicationnode.connect.biz.v1.mattermost;

import lombok.Data;

@Data
public class mmProject {

    private String me;
    private String teamId;

}
