package com.sparwk.communicationnode.connect.jpa.entity;

import com.sparwk.communicationnode.connect.jpa.repository.ViewUserInfoRepository;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_connect_list")
@DynamicUpdate
public class ViewConnectList {

    @Column(name = "profile_type ", nullable = true)
    private String profileType;
    @Id
    @Column(name = "profile_id ", nullable = true)
    private Long profileId;
    @Column(name = "profile_img ", nullable = true)
    private String profileImg;
    @Column(name = "profile_name ", nullable = true)
    private String profileName;
    @Column(name = "website ", nullable = true)
    private String website;
    @Column(name = "location_cd ", nullable = true)
    private String locationCd;
    @Column(name = "city ", nullable = true)
    private String city;
    @Column(name = "email ", nullable = true)
    private String email;
    @Column(name = "phone_number ", nullable = true)
    private String phoneNumber;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ViewConnectList(
            String profileType,
            Long profileId,
            String profileImg,
            String profileName,
            String website,
            String locationCd,
            String city,
            String email,
            String phoneNumber,
            LocalDateTime modDt
    ) {
        this.profileType = profileType;
        this.profileId = profileId;
        this.profileImg = profileImg;
        this.profileName = profileName;
        this.website = website;
        this.locationCd = locationCd;
        this.city = city;
        this.email = email;
        this.phoneNumber = phoneNumber;
        this.modDt = modDt;
    }

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ViewUserInfo.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private ViewUserInfo viewUserInfo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ViewCompanyInfo.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private ViewCompanyInfo ViewCompanyInfo;

    @OneToOne(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ViewGroupInfo.class)
    @JoinColumn(name = "profile_id", referencedColumnName = "profile_id")
    private ViewGroupInfo ViewGroupInfo;
/*

CREATE OR REPLACE VIEW sparwk01.view_connect_list
 AS
 SELECT 'user'::text AS profile_type,
    up.profile_id,
    up.profile_img_url AS profile_img,
    up.full_name AS profile_name,
    'user'::text AS website,
    up.current_city_country_cd AS location_cd,
    up.current_city_nm AS city,
    ua.accnt_email AS email,
    ua.phone_number,
    up.mod_dt
   FROM tb_profile up
     LEFT JOIN tb_account ua ON up.accnt_id = ua.accnt_id
     LEFT JOIN tb_account_passport ap ON up.accnt_id = ap.accnt_id
UNION ALL
 SELECT 'company'::text AS profile_type,
    cp.profile_id,
    cp.profile_img_url AS profile_img,
    cad.company_name AS profile_name,
    cp.com_info_website AS website,
    cal.location_cd,
    cad.city,
    ca.accnt_email AS email,
    'company'::text AS phone_number,
    cp.mod_dt
   FROM tb_profile_company cp
     LEFT JOIN tb_account ca ON cp.accnt_id = ca.accnt_id
     LEFT JOIN tb_account_company_detail cad ON cp.accnt_id = cad.accnt_id
     LEFT JOIN tb_account_company_location cal ON cp.accnt_id = cal.accnt_id
UNION ALL
 SELECT 'group'::text AS profile_type,
    pg.profile_id,
    pg.profile_img_url AS profile_img,
    pg.profile_group_name AS profile_name,
    pg.web_site AS website,
    agd.location_cd,
    agd.city,
    ga.accnt_email AS email,
    pgc.contct_phone_number AS phone_number,
    pg.mod_dt
   FROM tb_profile_group pg
     LEFT JOIN tb_account ga ON pg.accnt_id = ga.accnt_id
     LEFT JOIN tb_account_group_detail agd ON pg.accnt_id = agd.accnt_id
     LEFT JOIN tb_profile_group_contact pgc ON pg.profile_id = pgc.profile_id;
 */
}
