package com.sparwk.communicationnode.connect.jpa.entity;

import com.sparwk.communicationnode.connect.jpa.entity.id.ProfileLanguageId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_language")
@DynamicUpdate
@IdClass(ProfileLanguageId.class)
public class ProfileLanguage {

    @Id
    @Column(name = "language_cd",length = 9)
    private String languageCd;
    @Id
    @Column(name = "profile_id")
    private Long profileId;


    @Builder
    ProfileLanguage(
            Long profileId,
            String languageCd
    ) {
        this.profileId = profileId;
        this.languageCd = languageCd;
    }

}
