package com.sparwk.communicationnode.connect.jpa.repository;


import com.sparwk.communicationnode.connect.jpa.entity.ProfileAnrPitching;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileAnrPitchingRepository extends JpaRepository<ProfileAnrPitching, Long> {
//
//    @Transactional
//    @Modifying
//    @Query(
//            value = "UPDATE tb_profile_anr_pitching set pitching_yn = :pitchingYn " +
//                    "where profile_id = :profileId "
//            ,nativeQuery = true
//    )
//    int UpdateProfileUpdate(@Param("profileId")Long profileId, @Param("pitchingYn") String pitchingYn);

    boolean existsByProfileId(Long profileId);

    List<ProfileAnrPitching> findByPitchingYn(String yn);


}
