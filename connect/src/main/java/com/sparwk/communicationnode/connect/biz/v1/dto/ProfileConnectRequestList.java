package com.sparwk.communicationnode.connect.biz.v1.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectRequestList {

    List<ProfileConnectRequest> profileConnectRequestList;
}
