package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.AccountCompanyLocation;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountCompanyLocationRepository extends JpaRepository<AccountCompanyLocation, Long> {

    List<AccountCompanyLocation> findByLocationCdIn(List<String> cd);

}
