package com.sparwk.communicationnode.connect.jpa.entity;


import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "view_company_info")
@DynamicUpdate
public class ViewCompanyInfo {

    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "profile_company_name", nullable = true)
    private String profileCompanyName;
    @Column(name = "headline", nullable = true)
    private String headline;
    @Column(name = "location_cd", nullable = true)
    private String locationCd;
    @Column(name = "region", nullable = true)
    private String region;
    @Column(name = "city", nullable = true)
    private String city;
    @Column(name = "profile_img_url", nullable = true)
    private String profileImgUrl;
    @Column(name = "web_site", nullable = true)
    private String webSite;
    @Column(name = "mattermost_id", nullable = true)
    private String mattermostId;
    @Column(name = "accnt_email", nullable = true)
    private String accntEmail;
    @Column(name = "permision_type", nullable = true)
    private String permisionType;
    @Column(name = "profile_verify_yn", nullable = true)
    private String profileVerifyYn;

    @Builder
    ViewCompanyInfo(Long profileId,
                    String profileCompanyName,
                    String headline,
                    String locationCd,
                    String region,
                    String city,
                    String profileImgUrl,
                    String webSite,
                    String mattermostId,
                    String accntEmail,
                    String permisionType,
                    String profileVerifyYn
    ) {
        this.profileId = profileId;
        this.profileCompanyName = profileCompanyName;
        this.headline = headline;
        this.locationCd = locationCd;
        this.region = region;
        this.city = city;
        this.profileImgUrl = profileImgUrl;
        this.webSite = webSite;
        this.mattermostId = mattermostId;
        this.accntEmail = accntEmail;
        this.permisionType = permisionType;
        this.profileVerifyYn = profileVerifyYn;
    }
    /*

CREATE OR REPLACE VIEW sparwk01.view_company_info
 AS
 SELECT pc.profile_id,
    pc.profile_company_name,
    pc.headline,
    acl.location_cd,
    acd.region,
    acd.city,
    pc.profile_img_url,
    pc.com_info_website AS web_site,
    pc.mattermost_id,
    aa.accnt_email,
    ( SELECT string_agg(acta.company_cd::text, ','::text) AS string_agg
           FROM tb_account_company_type acta
             LEFT JOIN tb_account_company_type act ON pc.accnt_id = act.accnt_id
          WHERE acta.accnt_id = pc.accnt_id AND acta.company_license_verify_yn = 'Y'::bpchar) AS permision_type,
    ( SELECT
                CASE
                    WHEN tact.company_license_verify_yn = 'Y'::bpchar THEN 'Y'::text
                    ELSE 'N'::text
                END AS "case"
           FROM tb_account_company_type tact
          WHERE tact.accnt_id = aa.accnt_id
          GROUP BY tact.company_license_verify_yn) AS profile_verify_yn
   FROM tb_profile_company pc
     LEFT JOIN tb_account aa ON pc.accnt_id = aa.accnt_id
     LEFT JOIN tb_account_company_detail acd ON pc.accnt_id = acd.accnt_id
     LEFT JOIN tb_account_company_location acl ON pc.accnt_id = acl.accnt_id;
     */

}
