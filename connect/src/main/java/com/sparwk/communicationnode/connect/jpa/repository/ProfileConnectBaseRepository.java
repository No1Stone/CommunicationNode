package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.dto.ProfileConnectBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.ProfileConnect;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfileConnectBaseRepository {

    @Autowired
    private ModelMapper modelMapper;

    @Autowired
    private ProfileConnectRepository profileConnectRepository;


    public ProfileConnectBaseDTO ProfileConnectSaveService(ProfileConnectBaseDTO DTO) {
        ProfileConnect entity = modelMapper.map(DTO, ProfileConnect.class);
        profileConnectRepository.save(entity);
        ProfileConnectBaseDTO result = modelMapper.map(entity, ProfileConnectBaseDTO.class);
        return result;
    }

    public List<ProfileConnectBaseDTO> ProfileConnectSelectService(Long profileId) {
        return profileConnectRepository.findByProfileIdOrderByBookmarkYnAscRegDtDesc(profileId).stream()
                .map(e -> modelMapper.map(e, ProfileConnectBaseDTO.class)).collect(Collectors.toList());
    }

    public Long ProfileConnectDynamicUpdateService(ProfileConnectBaseDTO entity) {
        long result = profileConnectRepository.ProfileConnectDynamicUpdate(entity);
        return result;
    }


}
