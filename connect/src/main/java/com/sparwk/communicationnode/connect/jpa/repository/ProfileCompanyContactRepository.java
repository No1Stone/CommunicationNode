package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileCompanyContact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyContactRepository extends JpaRepository<ProfileCompanyContact, Long> {

    List<ProfileCompanyContact> findByCountryCdIn(List<String> cd);

}
