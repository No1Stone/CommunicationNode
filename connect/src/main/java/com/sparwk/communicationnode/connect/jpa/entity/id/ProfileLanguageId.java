package com.sparwk.communicationnode.connect.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileLanguageId implements Serializable {
    private String languageCd;
    private Long profileId;
}
