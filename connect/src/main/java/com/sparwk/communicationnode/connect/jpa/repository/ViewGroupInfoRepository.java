package com.sparwk.communicationnode.connect.jpa.repository;


import com.sparwk.communicationnode.connect.jpa.entity.ViewGroupInfo;
import com.sparwk.communicationnode.connect.jpa.entity.ViewUserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewGroupInfoRepository extends JpaRepository<ViewGroupInfo, Long> {
    Page<ViewGroupInfo> findByProfileIdIn(List<Long> profile , Pageable pr);
}
