package com.sparwk.communicationnode.connect.jpa.entity;

import com.sparwk.communicationnode.connect.jpa.entity.id.ProfileInterestMetaId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;
import org.springframework.data.annotation.CreatedDate;
import org.springframework.data.annotation.LastModifiedDate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_interest_meta")
@DynamicUpdate
@IdClass(ProfileInterestMetaId.class)
public class ProfileInterestMeta {


    @Id
    @Column(name = "kind_type_cd", nullable = true, length = 9)
    private String kindTypeCd;
    @Id
    @Column(name = "profile_id", length = 9)
    private Long profileId;
    @Id
    @Column(name = "detail_type_cd", nullable = true, length = 9)
    private String detailTypeCd;

    @Column(name = "reg_usr", updatable = false)
    //first registrant
    private Long regUsr;
    @Column(name = "reg_dt", updatable = false)
    @CreatedDate//serverTime
    //@CreationTimestamp//dbtime
    private LocalDateTime regDt;
    @Column(name = "mod_usr")
    private Long modUsr;
    @Column(name = "mod_dt")
    @LastModifiedDate//serverTime
    //@UpdateTimestamp//dbtime
    private LocalDateTime modDt;

    @Builder
    ProfileInterestMeta(
            Long profileId,
            String kindTypeCd,
            String detailTypeCd,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt

    ) {
        this.profileId = profileId;
        this.kindTypeCd = kindTypeCd;
        this.detailTypeCd = detailTypeCd;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }
}
