package com.sparwk.communicationnode.connect.biz.v1.dto;

import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewProfileListRequest {

    private int size;
    private int page;
    private List<Long> profileIdList;

}
