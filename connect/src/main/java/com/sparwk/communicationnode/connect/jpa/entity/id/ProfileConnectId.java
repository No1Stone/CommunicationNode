package com.sparwk.communicationnode.connect.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileConnectId implements Serializable {
    private Long profileId;
    private Long connectProfileId;
}
