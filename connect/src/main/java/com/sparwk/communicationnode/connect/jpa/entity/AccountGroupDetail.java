package com.sparwk.communicationnode.connect.jpa.entity;


import lombok.*;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_account_group_detail")
public class AccountGroupDetail {
    @Id
    @Column(name = "accnt_id", nullable = true)
    private Long accntId;
    @Column(name = "account_group_name", nullable = true)
    private String accountGroupName;
    @Column(name = "post_cd", nullable = true)
    private String postCd;
    @Column(name = "location_cd", nullable = true)
    private String locationCd;
    @Column(name = "region", nullable = true)
    private String region;
    @Column(name = "city", nullable = true)
    private String city;
    @Column(name = "addr1", nullable = true)
    private String addr1;
    @Column(name = "addr2", nullable = true)
    private String addr2;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    AccountGroupDetail(
            Long accntId,
            String accountGroupName,
            String postCd,
            String locationCd,
            String region,
            String city,
            String addr1,
            String addr2,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.accntId = accntId;
        this.accountGroupName = accountGroupName;
        this.postCd = postCd;
        this.locationCd = locationCd;
        this.region = region;
        this.city = city;
        this.addr1 = addr1;
        this.addr2 = addr2;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
