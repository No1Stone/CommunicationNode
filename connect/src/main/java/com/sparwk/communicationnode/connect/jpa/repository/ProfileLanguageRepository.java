package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileLanguage;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileLanguageRepository extends JpaRepository<ProfileLanguage, Long> {

    List<ProfileLanguage> findByLanguageCdInAndProfileIdIn(List<String> cd, List<Long> profileIdList);
}
