package com.sparwk.communicationnode.connect.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewConnectListBaseDTO {

    private String profileType;
    private Long profileId;
    private String profileImg;
    private String profileName;
    private String website;
    private String locationCd;
    private String city;
    private String email;
    private String phoneNumber;
    private LocalDateTime modDt;

}
