package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileGroupContact;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileGroupContactRepository extends JpaRepository<ProfileGroupContact, Long> {

    List<ProfileGroupContact> findByCountryCdIn(List<String> cd);
}
