package com.sparwk.communicationnode.connect.biz.v1.dto;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileInterestMeta;
import com.sparwk.communicationnode.connect.jpa.entity.ViewUserInfo;
import com.sparwk.communicationnode.connect.jpa.repository.ViewUserInfoRepository;
import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewUserInfoFliterResponse {
    private Long profileId;
    private String fullName;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String profileImgUrl;
    private String phoneNumber;
    private String language;
    private String mattermostId;
    private String accntEmail;
    private String permisionType;
    private String profileVerifyYn;
    @Schema(description = "그룹멤버 또는 유저소속회사")
    private Object cooperation;
    private List<ProfileInterestMeta> profileInterestMetaList;
    @Schema(description = "컨넥여부")
    private String connectYn;

}
