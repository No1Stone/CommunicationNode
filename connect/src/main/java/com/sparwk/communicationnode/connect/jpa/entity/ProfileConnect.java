package com.sparwk.communicationnode.connect.jpa.entity;

import com.sparwk.communicationnode.connect.jpa.entity.id.ProfileConnectId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_connect")
@DynamicUpdate
@IdClass(ProfileConnectId.class)
public class ProfileConnect {

    @Id
    @Column(name = "profile_id")
    private Long profileId;
    @Id
    @Column(name = "connect_profile_id")
    private Long connectProfileId;
    @Column(name = "connect_num", nullable = true)
    private Long connectNum;
    @Column(name = "profile_type", nullable = true)
    private String profileType;
    @Column(name = "bookmark_yn", nullable = true)
    private String bookmarkYn;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfileConnect(
            Long profileId,
            Long connectProfileId,
            Long connectNum,
            String profileType,
            String bookmarkYn,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.connectNum = connectNum;
        this.profileId = profileId;
        this.connectProfileId = connectProfileId;
        this.profileType = profileType;
        this.bookmarkYn = bookmarkYn;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
