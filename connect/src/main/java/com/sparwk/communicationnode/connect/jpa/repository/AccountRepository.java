package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.Account;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface AccountRepository  extends JpaRepository<Account, Long> {

    List<Account> findByCountryCdInAndAccntTypeCdAndAccntIdIn(List<String> countryCd, String type, List<Long>accountId);


}
