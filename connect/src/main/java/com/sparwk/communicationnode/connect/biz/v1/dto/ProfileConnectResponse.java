package com.sparwk.communicationnode.connect.biz.v1.dto;

import com.sparwk.communicationnode.connect.jpa.dto.ProfileConnectBaseDTO;
import lombok.*;

import javax.validation.constraints.NotNull;

@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProfileConnectResponse extends ProfileConnectBaseDTO {

    private String myMattermostId;
    private String connectMattermostId;
}
