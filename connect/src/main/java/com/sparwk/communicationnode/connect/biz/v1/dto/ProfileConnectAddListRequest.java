package com.sparwk.communicationnode.connect.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectAddListRequest {
    private int size;
    private int page;
    private String profileName;
    @Schema(description = "Common Code/ 국가, 핸드폰, 활동지역중 하나라도 Code가 있으면 검색됩니다.")
    private List<String> locationCdList;
    @Schema(description = "User Code")
    private List<String> interestMetaList;
    @Schema(description = "User Code")
    private List<String> languageList;
    @Schema(description = "type = user, company, group / null = all")
    private String userType;



}
