package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.Profile;
import com.sparwk.communicationnode.connect.jpa.entity.ProfilePosition;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfilePositionRepository extends JpaRepository<ProfilePosition, Long> {

    List<ProfilePosition> findByAnrYnOrderByModDtDesc(String yn);
    List<ProfilePosition> findByProfilePositionSeqInAndAndAnrYnAndCompanyVerifyYnOrderByModDtDesc(List<Long>seqList, String yn,String Yn);
    List<ProfilePosition> findByAnrYnAndCompanyVerifyYnOrderByModDtDesc(String anrYn,String comYn);


    List<ProfilePosition> findByCreatorYnOrderByModDtDesc(String yn);
    List<ProfilePosition> findByArtistYnOrderByModDtDesc(String yn);
    List<ProfilePosition> findByProfileId(Long profileId);
    List<ProfilePosition> findByProfileIdAndCompanyVerifyYnAndAnrYn(Long profileId,String comYn,String anrYn);
    boolean existsByProfileId(Long profileId);

}
