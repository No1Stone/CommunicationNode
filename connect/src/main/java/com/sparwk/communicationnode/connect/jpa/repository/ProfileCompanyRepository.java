package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileCompany;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyRepository extends JpaRepository<ProfileCompany, Long> {

    List<ProfileCompany> findByAccntIdIn(List<Long> accId);
}
