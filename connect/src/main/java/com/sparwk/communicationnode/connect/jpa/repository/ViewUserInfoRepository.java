package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ViewUserInfo;
import org.modelmapper.ModelMapper;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewUserInfoRepository extends JpaRepository<ViewUserInfo, Long> {

    Page<ViewUserInfo> findByProfileIdIn(List<Long>profile , Pageable pr);
    Page<ViewUserInfo> findByProfileIdInAndFullNameLike(List<Long>profile,String name , Pageable pr);
    Page<ViewUserInfo> findByProfileIdInAndFullNameLikeOrderByFullNameAsc(List<Long> permissionList, String name, Pageable pr);
    Page<ViewUserInfo> findByProfileIdInOrderByFullNameAsc(List<Long> permissionList,  Pageable pr);
}
