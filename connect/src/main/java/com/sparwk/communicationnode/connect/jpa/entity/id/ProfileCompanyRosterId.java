package com.sparwk.communicationnode.connect.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfileCompanyRosterId implements Serializable {
    private Long rosterSeq;
    private Long profileId;
}
