package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileCompanyPartner;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileCompanyPartnerRepository extends JpaRepository<ProfileCompanyPartner, Long> {

    //SELECT * FROM sparwk01.tb_profile_company_partner where profile_id = 10000000251
    // and relation_status = 'RES000003';
    List<ProfileCompanyPartner> findByProfileIdAndRelationStatus(Long profileId, String relation);

}
