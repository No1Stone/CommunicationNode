package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.ViewCompanyInfo;
import com.sparwk.communicationnode.connect.jpa.entity.ViewUserInfo;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ViewCompanyInfoRepository extends JpaRepository<ViewCompanyInfo, Long> {
    Page<ViewCompanyInfo> findByProfileIdIn(List<Long> profile , Pageable pr);
}
