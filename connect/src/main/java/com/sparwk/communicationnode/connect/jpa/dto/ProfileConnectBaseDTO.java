package com.sparwk.communicationnode.connect.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectBaseDTO {

    private Long profileId;
    private Long connectProfileId;
    private Long connectNum;
    private String profileType;
    private String bookmarkYn;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;

}
