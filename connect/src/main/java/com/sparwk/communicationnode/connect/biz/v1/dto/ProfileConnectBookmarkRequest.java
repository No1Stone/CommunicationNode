package com.sparwk.communicationnode.connect.biz.v1.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectBookmarkRequest {

    private Long profileId;
    private Long connectProfileId;
    private String bookmarkYn;;

}
