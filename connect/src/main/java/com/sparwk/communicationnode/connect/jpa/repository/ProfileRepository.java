package com.sparwk.communicationnode.connect.jpa.repository;

import com.sparwk.communicationnode.connect.jpa.entity.Profile;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfileRepository extends JpaRepository<Profile, Long> {
    List<Profile> findByProfileIdIn(List<Long> profileList);
    List<Profile> findByAccntIdInAndProfileIdIn(List<Long> accountList,List<Long> profileList);
    List<Profile> findByCurrentCityCountryCdInAndProfileIdIn(List<String> cd,List<Long> profileList);
    List<Profile> findByVerifyYnOrderByModDtDesc(String yn);
    List<Profile> findByOrderByModDtDesc();

}
