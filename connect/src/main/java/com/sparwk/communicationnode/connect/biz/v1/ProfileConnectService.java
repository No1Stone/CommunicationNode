package com.sparwk.communicationnode.connect.biz.v1;

import com.google.gson.Gson;
import com.sparwk.communicationnode.connect.biz.v1.dto.*;
import com.sparwk.communicationnode.connect.biz.v1.mattermost.MmFromTo;
import com.sparwk.communicationnode.connect.config.common.RestCall;
import com.sparwk.communicationnode.connect.config.filter.responsepack.Response;
import com.sparwk.communicationnode.connect.config.filter.responsepack.ResultCodeConst;
import com.sparwk.communicationnode.connect.config.filter.token.UserInfoDTO;
import com.sparwk.communicationnode.connect.jpa.dto.ProfileConnectBaseDTO;
import com.sparwk.communicationnode.connect.jpa.dto.ViewConnectListBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.ViewCompanyInfo;
import com.sparwk.communicationnode.connect.jpa.entity.ViewConnectList;
import com.sparwk.communicationnode.connect.jpa.repository.*;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Slice;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfileConnectService {

    private final ModelMapper modelMapper;
    private final MessageSourceAccessor messageSourceAccessor;
    private final HttpServletRequest httpServletRequest;
    private final Logger logger = LoggerFactory.getLogger(ProfileConnectService.class);
    private final ProfileConnectBaseRepository profileConnectBaseRepository;
    private final ProfileConnectRepository profileConnectRepository;
    private final ViewConnectListRepository viewConnectListRepository;
    private final ViewCompanyInfoRepository viewCompanyInfoRepository;
    private final ViewGroupInfoRepository viewGroupInfoRepository;
    private final ViewUserInfoRepository viewUserInfoRepository;


    private final ProfileGroupContactRepository profileGroupContactRepository;
    private final ProfileGroupRepository profileGroupRepository;
    private final ProfileCompanyContactRepository profileCompanyContactRepository;
    private final ProfileCompanyRepository profileCompanyRepository;
    private final ProfileRepository profileRepository;
    private final AccountPassportRepository accountPassportRepository;
    private final AccountGroupDetailRepository accountGroupDetailRepository;
    private final AccountCompanyLocationRepository accountCompanyLocationRepository;
    private final AccountRepository accountRepository;
    private final ProfileInterestMetaRepository profileInterestMetaRepository;
    private final ProfileLanguageRepository profileLanguageRepository;
    private final ProfilePositionRepository profilePositionRepository;
    private final ProfileCompanyPartnerRepository profileCompanyPartnerRepository;
    private final ProfileCompanyRosterRepository profileCompanyRosterRepository;
    private final ProfileAnrPitchingRepository profileAnrPitchingRepository;
    @Value("${sparwk.node.communication.chat}")
    private String chat;
    @Value("${sparwk.node.schema}")
    private String schema;
    @Value("${sparwk.node.personalization.profileip}")
    private String profileIp;

    public Response ProfileConnectSaveService(ProfileConnectRequest dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        ProfileConnectBaseDTO baseDTO = modelMapper.map(dto, ProfileConnectBaseDTO.class);
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        baseDTO.setModUsr(userInfoDTO.getAccountId());
        baseDTO.setRegUsr(userInfoDTO.getAccountId());
        baseDTO.setBookmarkYn("N");
        baseDTO.setUseYn("Y");

        MmFromTo mm = new MmFromTo();
        mm.setFromId(dto.getProfileId());
        mm.setToId(dto.getConnectProfileId());
        logger.info("mm - -{}", new Gson().toJson(mm));
        String mmRes = RestCall.postRes(mm.getBaseURL() + "/user/add/connect", new Gson().toJson(mm));
        logger.info("mm - -{}", mmRes);


        ProfileConnectBaseDTO result = profileConnectBaseRepository.ProfileConnectSaveService(baseDTO);
        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectListSaveService(ProfileConnectRequestList dto) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) httpServletRequest.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        List<ProfileConnectBaseDTO> resultList = new ArrayList<>();
        for (ProfileConnectRequest e : dto.getProfileConnectRequestList()) {
            ProfileConnectBaseDTO baseDTO = modelMapper.map(e, ProfileConnectBaseDTO.class);
            baseDTO.setModDt(LocalDateTime.now());
            baseDTO.setRegDt(LocalDateTime.now());
            baseDTO.setModUsr(userInfoDTO.getAccountId());
            baseDTO.setRegUsr(userInfoDTO.getAccountId());
            ProfileConnectBaseDTO result = profileConnectBaseRepository.ProfileConnectSaveService(baseDTO);
            resultList.add(result);
        }
        Response res = new Response();
        res.setResult(resultList);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectSelectService(Long profileId) {
//        List<ProfileConnectBaseDTO> result = profileConnectRepository
        List<ProfileConnectResponse> result = profileConnectRepository.findByProfileIdOrderByBookmarkYnDesc(profileId)
//                .stream().map(e -> modelMapper.map(e, ProfileConnectBaseDTO.class))
                .stream().map(e -> modelMapper.map(e, ProfileConnectResponse.class))
                .peek(e -> {
                    e.setMyMattermostId(profileRepository.findById(e.getProfileId()).orElse(null).getMattermostId());
                    e.setConnectMattermostId(profileRepository.findById(e.getConnectProfileId()).orElse(null).getMattermostId());
                })
                .collect(Collectors.toList());

//        List<ProfileConnectResponse> response = result.stream().map(e ->
//                        modelMapper.map(e, ProfileConnectResponse.class)
//                )
//                .peek(e -> {
//                    e.setMyMattermostId(profileRepository.findById(e.getProfileId()).orElse(null).getMattermostId())
//                    e.setConnectMattermostId(profileRepository.findById(e.getConnectProfileId()).orElse(null).getMattermostId());
//                })
//                .collect(Collectors.toList());

        Response res = new Response();
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectListdetailSelectService(ViewProfileRequest dto) {
        List<Long> result = profileConnectRepository.findByProfileIdOrderByBookmarkYnAscRegDtDesc(dto.getProfileId())
                .stream().map(e -> modelMapper.map(e.getConnectProfileId(), Long.class)).collect(Collectors.toList());
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        res.setResult(viewConnectListRepository.findByProfileIdIn(result, pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectdeleteService(ProfileConnectDeleteRequest dto) {
        Response res = new Response();
        profileConnectRepository.deleteByProfileIdAndConnectProfileId(dto.getProfileId(), dto.getConnectProfileId());

        MmFromTo mm = new MmFromTo();
        mm.setFromId(dto.getProfileId());
        mm.setToId(dto.getConnectProfileId());
        logger.info("mm - -{}", new Gson().toJson(mm));
        String mmRes = RestCall.postRes(mm.getBaseURL() + "/user/delete/connect", new Gson().toJson(mm));
        logger.info("mm - -{}", mmRes);

        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectBoolmarkUpdateService(ProfileConnectBookmarkRequest dto) {
        Response res = new Response();
        ProfileConnectBaseDTO pc = ProfileConnectBaseDTO
                .builder()
                .profileId(dto.getProfileId())
                .connectProfileId(dto.getConnectProfileId())
                .bookmarkYn(dto.getBookmarkYn())
                .build();
        long saveResult = profileConnectRepository.ProfileConnectDynamicUpdate(pc);
        res.setResult(saveResult);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectAddListService(ProfileConnectAddListRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        logger.info("--name {}", dto.getProfileName().length());
        logger.info("--List {}", dto.getLocationCdList().size());
        List<ViewConnectListBaseDTO> result = new ArrayList<>();
        if (dto.getProfileName().length() > 0 && dto.getLocationCdList().size() == 0
//                ||
//                dto.getProfileName() != null && dto.getLocationCdList() == null
//                || !dto.getProfileName().isEmpty() && dto.getLocationCdList().isEmpty()
        ) {
            logger.info("1");
            Slice<ViewConnectList> List = viewConnectListRepository
                    .findByProfileNameLike("%" + dto.getProfileName() + "%", pr);
            res.setResult(List);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else if (dto.getProfileName().length() == 0 && dto.getLocationCdList().size() > 0
//                dto.getProfileName() == null && dto.getLocationCdList() != null
//                || dto.getProfileName().isEmpty() && !dto.getLocationCdList().isEmpty()
        ) {
            logger.info("2");
            Slice<ViewConnectList> List = viewConnectListRepository.findAllByLocationCdIn(dto.getLocationCdList(), pr);
            res.setResult(List);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else if (dto.getProfileName().length() > 0 && dto.getLocationCdList().size() > 0
//                dto.getProfileName() != null && dto.getLocationCdList() != null
//                || !dto.getProfileName().isEmpty() && !dto.getLocationCdList().isEmpty()
        ) {
            logger.info("3");
            Slice<ViewConnectList> List = viewConnectListRepository
                    .findByProfileNameLikeAndLocationCdIn
                            ("%" + dto.getProfileName() + "%", dto.getLocationCdList(), pr);
            res.setResult(List);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            logger.info("4");
            Slice<ViewConnectList> list = viewConnectListRepository.findAll(pr);
            res.setResult(list);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
    }

    /*
        public Response ProfileConnectAddListFilterService(ProfileConnectAddListRequest dto) {
            Response res = new Response();
            PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
            logger.info(" = = = ={}", dto);
            Slice<ViewConnectList> list = viewConnectListRepository.findAll(pr);
            List<Long> profileIdList = new ArrayList<>();

            //유저
            if (dto.getUserType().equals("user")) {

                if (
                        dto.getLocationCdList().isEmpty() &&
                                dto.getLanguageList().isEmpty() &&
                                dto.getInterestMetaList().isEmpty()
                ) {
                    profileIdList = viewConnectListRepository.findByProfileType("user")
                            .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                            .collect(Collectors.toList());
                } else {

                    if (
                            dto.getLocationCdList() != null ||
                                    !dto.getLocationCdList().isEmpty()) {
                        for (Long e : UserCountryCdSelectList(dto.getLocationCdList())) {
                            profileIdList.add(e);
                        }
                    }
                    if (
                            dto.getLanguageList() != null ||
                                    !dto.getLanguageList().isEmpty()) {
                        for (Long e : UserLangCountryCdSelectList(dto.getLanguageList())) {
                            profileIdList.add(e);
                        }
                    }
                    if (
                            dto.getInterestMetaList() != null ||
                                    !dto.getInterestMetaList().isEmpty()) {
                        for (Long e : UserMetaCountryCdSelectList(dto.getInterestMetaList())) {
                            profileIdList.add(e);
                        }
                    }
                }
            } else if (dto.getUserType().equals("company")) {      // 컴퍼니
                if (
                        dto.getLocationCdList() == null ||
                                dto.getLocationCdList().isEmpty()) {

                    profileIdList = viewConnectListRepository.findByProfileType("company")
                            .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                            .collect(Collectors.toList());
                } else {
                    for (Long e : CompanyCountryCdSelectList(dto.getLocationCdList())) {
                        profileIdList.add(e);
                    }
                }
            } else if (dto.getUserType().equals("group")) {
                //그룹
                if (
                        dto.getLocationCdList() == null ||
                                dto.getLocationCdList().isEmpty()) {
                    profileIdList = viewConnectListRepository.findByProfileType("group")
                            .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                            .collect(Collectors.toList());
                } else {
                    for (Long e : GroupCountryCdSelectList(dto.getLocationCdList())) {
                        profileIdList.add(e);
                    }
                }
            } else {
                logger.info("디폴트");
                profileIdList = viewConnectListRepository.findAllByOrderByModDtDesc().stream().map(e ->
                        modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
            }

            //결과를 리턴
            if (dto.getProfileName() == null) {
                list = viewConnectListRepository.findByProfileIdInOrderByModDtDesc(profileIdList, pr);
                res.setResult(list);
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            } else {
                list = viewConnectListRepository
                        .findByProfileIdInAndProfileNameLikeOrderByModDtDesc(profileIdList, "%" + dto.getProfileName() + "%", pr);
                res.setResult(list);
                res.setResultCd(ResultCodeConst.SUCCESS.getCode());
                res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
                return res;
            }
        }
    */
    public Response ProfileConnectUserFilterService(ProfileConnectUserRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        logger.info(" = = = ={}", dto);
        Slice<ViewConnectList> list = viewConnectListRepository.findAll(pr);
        List<Long> permissionList = new ArrayList<>();
        if (dto.getPermissionType().equals("creator")) {
            permissionList = profileRepository.findByOrderByModDtDesc().stream().map(e ->
                    modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        }
        if (dto.getPermissionType().equals("anr")) {
            permissionList = profilePositionRepository.findByAnrYnAndCompanyVerifyYnOrderByModDtDesc("Y", "Y").stream().map(e ->
                    modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        }
//        if (dto.getPermissionType().equals("artist")) {
//            permissionList = profilePositionRepository.findByArtistYnOrderByModDtDesc("Y").stream().map(e ->
//                    modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
//        }

        logger.info(" perrmission------{} ", permissionList);

        List<Long> filterList = new ArrayList<>();
        if (dto.getLocationCdList() == null ||
                dto.getLocationCdList().size() == 0 || dto.getLocationCdList().isEmpty() || dto.getLocationCdList().equals("")) {
        } else {
            for (Long e : UserCountryCdSelectList(dto.getLocationCdList(), permissionList)) {
                filterList.add(e); //and where
                permissionList.add(e);//or where
            }
//            permissionList.clear();  //and where
//            permissionList = new ArrayList<>();  //and where
//            permissionList = filterList;  //and where
//            filterList = new ArrayList<>();  //and where
            logger.info(" filter0------{} ", permissionList);
        }
        if (dto.getLanguageList() == null ||
                dto.getLanguageList().size() == 0 || dto.getLanguageList().isEmpty() || dto.getLanguageList().equals("")) {
        } else {
            for (Long e : UserLangCountryCdSelectList(dto.getLanguageList(), permissionList)) {
                filterList.add(e);//and where
                permissionList.add(e); //or where
            }
//            permissionList.clear();//and where
//            permissionList = new ArrayList<>();//and where
//            permissionList = filterList;//and where
//            filterList = new ArrayList<>();//and where
            logger.info(" filter1------{} ", permissionList);
        }
        if (dto.getInterestMetaList() == null ||
                dto.getInterestMetaList().size() == 0 || dto.getInterestMetaList().isEmpty() || dto.getInterestMetaList().equals("")) {
        } else {
            for (Long e : UserMetaCountryCdSelectList(dto.getInterestMetaList(), permissionList)) {
                filterList.add(e); //and where
                permissionList.add(e); //or where
            }
//            permissionList.clear();//and where
//            permissionList = new ArrayList<>();//and where
//            permissionList = filterList;//and where
//            filterList = new ArrayList<>();//and where
            logger.info(" filter2------{} ", permissionList);
        }

        permissionList.stream().distinct().collect(Collectors.toList());
        logger.info(" filter3------{} ", permissionList);
        if (filterList.size() < 1) {
            filterList = permissionList.stream().distinct().collect(Collectors.toList());
        }

        // 피칭이 N인애들을 지운다
        List<Long> AnrNList = profileAnrPitchingRepository.findByPitchingYn("N")
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .collect(Collectors.toList());

        permissionList.removeAll(AnrNList);
        filterList.removeAll(AnrNList);

        if (dto.getProfileName() == null || dto.getProfileName().isEmpty()) {
            Page<ViewUserInfoFliterResponse> respon =
                    viewUserInfoRepository.findByProfileIdInOrderByFullNameAsc(filterList, pr)
                            .map(e -> modelMapper.map(e, ViewUserInfoFliterResponse.class));
            respon.getContent().stream().forEach(e -> {
                List<Long> connect = new ArrayList<>();
                e.setProfileInterestMetaList(profileInterestMetaRepository.findByProfileId(e.getProfileId()));
                if (profilePositionRepository.existsByProfileId(e.getProfileId())) {
                    List<Long> comList = profilePositionRepository.findByProfileIdAndCompanyVerifyYnAndAnrYn(e.getProfileId(), "Y", "Y")
                            .stream().filter(i -> i.getCompanyProfileId() != null)
                            .map(i -> modelMapper.map(i.getCompanyProfileId(), Long.class))
                            .collect(Collectors.toList());
                    List<ViewCompanyInfo> viewCom = new ArrayList<>();
                    for (Long l : comList) {
                        viewCom.add(viewCompanyInfoRepository.findById(l).orElse(null));
                    }
                    e.setCooperation(viewCom);
                }
                if (dto.getProfileId() != null) {
                    if (profileConnectRepository.existsByProfileIdAndConnectProfileId(dto.getProfileId(), e.getProfileId())) {
                        e.setConnectYn("Y");
                    } else {
                        e.setConnectYn("N");
                    }
                }
            });

            res.setResult(respon);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            Page<ViewUserInfoFliterResponse> respon = viewUserInfoRepository
                    .findByProfileIdInAndFullNameLikeOrderByFullNameAsc(filterList, "%" + dto.getProfileName() + "%", pr)
                    .map(e -> modelMapper.map(e, ViewUserInfoFliterResponse.class));
            respon.getContent().stream().forEach(e -> {
                e.setProfileInterestMetaList(profileInterestMetaRepository.findByProfileId(e.getProfileId()));
                if (profilePositionRepository.existsByProfileId(e.getProfileId())) {
                    List<Long> comList = profilePositionRepository.findByProfileIdAndCompanyVerifyYnAndAnrYn(e.getProfileId(), "Y", "Y")
                            .stream().filter(i -> i.getCompanyProfileId() != null)
                            .map(i -> modelMapper.map(i.getCompanyProfileId(), Long.class))
                            .collect(Collectors.toList());
                    List<ViewCompanyInfo> viewCom = new ArrayList<>();
                    for (Long l : comList) {
                        viewCom.add(viewCompanyInfoRepository.findById(l).orElse(null));
                    }
                    e.setCooperation(viewCom);
                }
                if (dto.getProfileId() != null) {
                    if (profileConnectRepository.existsByProfileIdAndConnectProfileId(dto.getProfileId(), e.getProfileId())) {
                        e.setConnectYn("Y");
                    } else {
                        e.setConnectYn("N");
                    }
                }
            });
            res.setResult(respon);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
/*
        if (dto.getProfileName() == null || dto.getProfileName().isEmpty()) {
            Slice<ViewUserInfo> viewResponse = viewUserInfoRepository.findByProfileIdIn(permissionList, pr);
            res.setResult(viewResponse);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        } else {
            Slice<ViewUserInfo> viewResponse = viewUserInfoRepository
                    .findByProfileIdInAndFullNameLike(permissionList, "%" + dto.getProfileName() + "%", pr);
            res.setResult(viewResponse);
            res.setResultCd(ResultCodeConst.SUCCESS.getCode());
            res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
            return res;
        }
*/
    }

    private List<Long> UserCountryCdSelectList(List<String> cdList, List<Long> permissionList) {
        Response res = new Response();

        List<Long> accountInList = profileRepository.findByProfileIdIn(permissionList).stream().map(
                e -> modelMapper.map(e.getAccntId(), Long.class)).collect(Collectors.toList());
        List<Long> accountList = accountRepository.findByCountryCdInAndAccntTypeCdAndAccntIdIn(cdList, "user", accountInList)
                .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                .collect(Collectors.toList());
        List<Long> accountPassList = accountPassportRepository.findByCountryCdInAndAccntIdIn(cdList, accountInList)
                .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                .collect(Collectors.toList());
        List<Long> profileConvert = new ArrayList<>();
        for (Long e : accountList) {
            profileConvert.add(e);
        }
        for (Long e : accountPassList) {
            profileConvert.add(e);
        }
        List<Long> accountProfileList = profileRepository.findByAccntIdInAndProfileIdIn(profileConvert, permissionList)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .collect(Collectors.toList());
        List<Long> profileCdList = profileRepository.findByCurrentCityCountryCdInAndProfileIdIn(cdList, permissionList)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .collect(Collectors.toList());
        List<Long> profileIdList = new ArrayList<>();
        for (Long e : accountProfileList) {
            profileIdList.add(e);
        }
        for (Long e : profileCdList) {
            profileIdList.add(e);
        }
        return profileIdList.stream().distinct().collect(Collectors.toList());
    }

    /*
        private List<Long> CompanyCountryCdSelectList(List<String> cdList) {
            Response res = new Response();
            List<Long> accountList = accountRepository.findByCountryCdInAndAccntTypeCd(cdList, "company")
                    .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                    .collect(Collectors.toList());
            List<Long> companyDetailCd = accountCompanyLocationRepository.findByLocationCdIn(cdList)
                    .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                    .collect(Collectors.toList());
            List<Long> profileConvert = new ArrayList<>();
            for (Long e : accountList) {
                profileConvert.add(e);
            }
            for (Long e : companyDetailCd) {
                profileConvert.add(e);
            }
            List<Long> profileIdList = profileCompanyRepository.findByAccntIdIn(profileConvert)
                    .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                    .collect(Collectors.toList());

            List<Long> profileContactList = profileCompanyContactRepository.findByCountryCdIn(cdList)
                    .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
            for (Long e : profileContactList) {
                profileIdList.add(e);
            }
            return profileIdList.stream().distinct().collect(Collectors.toList());
        }
    *//*
    private List<Long> GroupCountryCdSelectList(List<String> cdList) {
        List<Long> accountList = accountRepository.findByCountryCdInAndAccntTypeCd(cdList, "group")
                .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class))
                .collect(Collectors.toList());
        List<Long> groupDetail = accountGroupDetailRepository.findByLocationCdIn(cdList)
                .stream().map(e -> modelMapper.map(e.getAccntId(), Long.class)).collect(Collectors.toList());
        List<Long> profileConvert = new ArrayList<>();
        for (Long e : accountList) {
            profileConvert.add(e);
        }
        for (Long e : groupDetail) {
            profileConvert.add(e);
        }
        List<Long> profileIdList = profileGroupRepository.findByAccntIdIn(profileConvert)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .collect(Collectors.toList());
        List<Long> conList = profileGroupContactRepository.findByCountryCdIn(cdList).stream().map(e ->
                        modelMapper.map(e.getProfileId(), Long.class))
                .collect(Collectors.toList());
        for (Long e : conList) {
            profileIdList.add(e);
        }
        return profileIdList.stream().distinct().collect(Collectors.toList());
    }
*/
    private List<Long> UserMetaCountryCdSelectList(List<String> cdList, List<Long> permissionList) {
        return profileInterestMetaRepository.findByDetailTypeCdInAndProfileIdIn(cdList, permissionList)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .distinct().collect(Collectors.toList());
    }

    private List<Long> UserLangCountryCdSelectList(List<String> cdList, List<Long> permissionList) {
        return profileLanguageRepository.findByLanguageCdInAndProfileIdIn(cdList, permissionList)
                .stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .distinct().collect(Collectors.toList());
    }

    public Response ProfileConnectViewConnectListInService(ViewProfileListRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        res.setResult(viewConnectListRepository.findByProfileIdInOrderByModDtDesc(dto.getProfileIdList(), pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewUserListInService(ViewProfileListRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        res.setResult(viewUserInfoRepository.findByProfileIdIn(dto.getProfileIdList(), pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewCompanyListInService(ViewProfileListRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        res.setResult(viewCompanyInfoRepository.findByProfileIdIn(dto.getProfileIdList(), pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewGroupListInService(ViewProfileListRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        res.setResult(viewGroupInfoRepository.findByProfileIdIn(dto.getProfileIdList(), pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewUserOneService(Long profileId) {
        Response res = new Response();
        res.setResult(viewUserInfoRepository.findById(profileId));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewCompanyOneService(Long profileId) {
        Response res = new Response();
        res.setResult(viewCompanyInfoRepository.findById(profileId));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectViewGroupOneService(Long profileId) {
        Response res = new Response();
        res.setResult(viewGroupInfoRepository.findById(profileId));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectListFilterSelectService(ProfileConnectAddListRequest dto, Long profileId) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        List<Long> myConnectList = profileConnectRepository.findByProfileIdOrderByModDt(profileId).stream().map(e ->
                modelMapper.map(e.getConnectProfileId(), Long.class)).collect(Collectors.toList());
        logger.info("my connectList = {}", myConnectList);
        List<Long> userTypeList = new ArrayList<>();
        if (dto.getUserType().equals("user")
                || dto.getUserType().equals("company")
                || dto.getUserType().equals("group")) {
            userTypeList = viewConnectListRepository
                    .findByProfileIdInAndProfileTypeOrderByModDtDesc(myConnectList, dto.getUserType())
                    .stream().map(e ->
                            modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        } else {
            userTypeList = viewConnectListRepository.findByProfileIdInOrderByModDtDesc(myConnectList)
                    .stream().map(e ->
                            modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        }
        logger.info("usertype List --{}", userTypeList);

        List<Long> filterList = new ArrayList<>();
        if (dto.getLanguageList().size() == 0
                || dto.getLanguageList().isEmpty()
                || dto.getLanguageList().equals("")
                || dto.getLanguageList() == null) {
        } else {
            for (Long e : UserLangCountryCdSelectList(dto.getLanguageList(), userTypeList)) {
                filterList.add(e);//and where
//                userTypeList.add(e); //or where
            }
            userTypeList.clear();//and where
            userTypeList = new ArrayList<>();//and where
            userTypeList = filterList;//and where
            filterList = new ArrayList<>();//and where
            logger.info(" filter1------{} ", userTypeList);
        }
        if (dto.getInterestMetaList().size() == 0
                || dto.getInterestMetaList().isEmpty()
                || dto.getInterestMetaList().equals("")
                || dto.getInterestMetaList() == null) {
        } else {
            for (Long e : UserMetaCountryCdSelectList(dto.getInterestMetaList(), userTypeList)) {
                filterList.add(e);//and where
//                userTypeList.add(e); //or where
            }
            userTypeList.clear();//and where
            userTypeList = new ArrayList<>();//and where
            userTypeList = filterList;//and where
            filterList = new ArrayList<>();//and where
            logger.info(" filter2------{} ", userTypeList);
        }

        logger.info("이름전 리스트 - {}", userTypeList);
        if (dto.getProfileName() == null || dto.getProfileName().isEmpty()) {
        } else {
            userTypeList = viewConnectListRepository
                    .findByProfileIdInAndProfileNameLikeOrderByModDtDesc
                            (userTypeList, "%" + dto.getProfileName() + "%").stream().map(e ->
                            modelMapper.map(e.getProfileId(), Long.class)).collect(Collectors.toList());
        }

        logger.info("리턴전리스트 - {}", userTypeList);
        res.setResult(
                profileConnectRepository.
                        findByProfileIdAndConnectProfileIdInOrderByBookmarkYnDesc(profileId, userTypeList, pr)
                        .stream().map(e -> modelMapper.map(e, ProfileConnectResponse.class))
                        .peek(e -> {
                            e.setMyMattermostId(profileRepository.findById(e.getProfileId()).orElse(null).getMattermostId());
                            e.setConnectMattermostId(profileRepository.findById(e.getConnectProfileId()).orElse(null).getMattermostId());
                        })
                        .collect(Collectors.toList())
        );
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfileConnectCompanyPartnerAnrListServuce(ViewProfileRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());
        //컴퍼니 프로필아이디로 tb_profile_company_partner 조회 =
        //SELECT * FROM sparwk01.tb_profile_company_partner where profile_id = 10000000251
        // and relation_status = 'RES000003';
        List<Long> companyProfileList = new ArrayList<>();
        companyProfileList = profileCompanyPartnerRepository.findByProfileIdAndRelationStatus
                        (dto.getProfileId(), "RES000003")
                .stream().map(e -> modelMapper.map(e.getPartnerId(), Long.class))
                .collect(Collectors.toList());
        ;
        //RET000001 : 파트너
        //RET000002 : 모회사
        //RET000003 : 자회사
        //RES000003 : 관계중/ 수락

        //조회후 내회사 프로필 추가
        companyProfileList.add(dto.getProfileId());
        //10000000201,10000000183,10000000182,10000000181,10000000251
        logger.info("companyProfileList = ={}", companyProfileList);
        //프로필 아이디 tb_profile_company_roster 테이블 조회
//        SELECT * FROM sparwk01.tb_profile_company_roster
//        where profile_id in(10000000201,10000000183,10000000182,10000000181,10000000251) and
//                roster_status = 'RSS000002';

        List<Long> positionSeqList = new ArrayList<>();
        positionSeqList = profileCompanyRosterRepository.findByProfileIdInAndRosterStatus
                        (companyProfileList, "RSS000002")
                .stream().map(e -> modelMapper.map(e.getProfilePositionSeq(), Long.class))
                .collect(Collectors.toList());
        logger.info("positionSeqList = ={}", positionSeqList);
        //388,303,306,304,302,305,387,388,387,363,371,410
        //시퀀스리스트로 tb_profile_position 조회 로스터를 맺은 상태이기에 별도의 조건없이 검색. anrProfileList
        //SELECT * FROM sparwk01.tb_profile_position where profile_position_seq in
        //	(388,303,306,304,302,305,387,388,387,363,371,410) and anr_yn = 'Y'
        //10000000109,10000000109,10000000109,10000000109,10000000249,10000000198,10000000272
        //중복 제거후 view_user_info 조회
        List<Long> anrProfileList = new ArrayList<>();
        anrProfileList = profilePositionRepository.findByProfilePositionSeqInAndAndAnrYnAndCompanyVerifyYnOrderByModDtDesc(
                        positionSeqList, "Y", "Y").stream().map(e -> modelMapper.map(e.getProfileId(), Long.class))
                .distinct().collect(Collectors.toList());
        logger.info("anrProfileList = ={}", anrProfileList);
        res.setResult(viewUserInfoRepository.findByProfileIdIn(anrProfileList, pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}


