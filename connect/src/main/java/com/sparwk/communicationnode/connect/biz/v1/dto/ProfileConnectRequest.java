package com.sparwk.communicationnode.connect.biz.v1.dto;

import io.swagger.v3.oas.annotations.media.Schema;
import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfileConnectRequest {
    private Long profileId;
    private Long connectProfileId;
    private Long connectNum;;
    private String profileType;

}
