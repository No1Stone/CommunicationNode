package com.sparwk.communicationnode.connect.jpa.repository.dsl;

import com.querydsl.core.QueryResults;
import com.querydsl.jpa.impl.JPAQuery;
import com.querydsl.jpa.impl.JPAQueryFactory;
import com.sparwk.communicationnode.connect.jpa.dto.ViewConnectListBaseDTO;
import com.sparwk.communicationnode.connect.jpa.entity.QProfileConnect;
import com.sparwk.communicationnode.connect.jpa.entity.QViewConnectList;
import lombok.RequiredArgsConstructor;
import org.aspectj.lang.annotation.Aspect;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.EntityManager;
import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Aspect
@RequiredArgsConstructor
public class ViewConnectListDslImpl implements ViewConnectListDsl{

    private EntityManager em;
    private JPAQueryFactory queryFactory;
    private ModelMapper modelMapper;
    private final Logger logger = LoggerFactory.getLogger(ViewConnectListDslImpl.class);
    private QViewConnectList qViewConnectList = QViewConnectList.viewConnectList;

    private ViewConnectListDslImpl(EntityManager em) {
        this.em = em;
        this.queryFactory = new JPAQueryFactory(em);
    }


    @Override
    public List<ViewConnectListBaseDTO> ListDynamicSelect(String name, List<String> location) {

        JPAQueryFactory queryFactorya = new JPAQueryFactory(em);
        List<ViewConnectListBaseDTO> result = new ArrayList<>();
        result = queryFactory.select(qViewConnectList).fetch()
                .stream().map(e-> modelMapper.map(e, ViewConnectListBaseDTO.class)).collect(Collectors.toList());



        return null;
    }
}
