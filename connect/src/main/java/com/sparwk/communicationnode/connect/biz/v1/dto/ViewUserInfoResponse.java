package com.sparwk.communicationnode.connect.biz.v1.dto;

import com.sparwk.communicationnode.connect.jpa.entity.ProfileInterestMeta;
import lombok.*;

import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ViewUserInfoResponse {
    private Long profileId;
    private String fullName;
    private String bio;
    private String headline;
    private String currentCityCountryCd;
    private String currentCityNm;
    private String profileImgUrl;
    private String phoneNumber;
    private String language;
    private String mattermostId;
    private String accntEmail;
    private String permisionType;
    private String profileVerifyYn;
    private List<ProfileInterestMeta> profileInterestMetaList;
}
