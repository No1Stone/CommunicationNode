package com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion;

import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.dto.ProfilePostCommentEmotionRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.responsepack.ResultCodeConst;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostCommentEmotionBaseDTO;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostBaseRepository;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostCommentEmotionRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfilePostCommentEmotionService {
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfilePostCommentEmotionService.class);
    @Autowired
    private ProfilePostBaseRepository profilePostBaseRepository;
    @Autowired
    private ProfilePostCommentEmotionRepository profilePostCommentEmotionRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Response ProfilePostCommentEmotionSaveService(ProfilePostCommentEmotionRequest dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO - {}", userInfoDTO.getAccountId());
        logger.info("userInfoDTO - {}", userInfoDTO.getLastUseProfileId());
        Response res = new Response();
        ProfilePostCommentEmotionBaseDTO baseDTO = modelMapper.map(dto, ProfilePostCommentEmotionBaseDTO.class);
        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setRegUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        ProfilePostCommentEmotionBaseDTO result = profilePostBaseRepository.ProfilePostCommentEmotionSaveService(baseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePostCommentEmotionSelectService(Long profileId) {

        Response res = new Response();
        List<ProfilePostCommentEmotionBaseDTO> result = profilePostBaseRepository
                .ProfilePostCommentEmotionSelectService(profileId);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

}
