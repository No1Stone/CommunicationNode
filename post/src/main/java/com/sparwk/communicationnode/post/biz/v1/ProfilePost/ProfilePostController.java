package com.sparwk.communicationnode.post.biz.v1.ProfilePost;

import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.ProfilePostRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/post")
@CrossOrigin("*")
@Api(tags = "Profile Server Post API")
public class ProfilePostController {
    @Autowired
    private ProfilePostService profilePostService;
    private final Logger logger = LoggerFactory.getLogger(ProfilePostController.class);

    @ApiOperation(
            value = "포스트 저장",
            notes = "포스트 저장"
    )
    @PostMapping(path = "/info")
    public Response ProfilePostSaveController(@Valid @RequestBody ProfilePostRequest dto, HttpServletRequest req) {
        logger.info("------{}",dto);
        Response res = profilePostService.ProfilePostSaveService(dto,req);
        return res;
    }
    @ApiOperation(
            value = "포스트 검색",
            notes = "포스트 검색"
    )
    @GetMapping(path = "/info")
    public Response ProfilePostSelectController(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = profilePostService.ProfilePostSelectService(userInfoDTO.getLastUseProfileId());
        return res;
    }

}
