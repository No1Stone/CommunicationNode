package com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class PostRequest {

    private Long profileId;
    private int size;
    private int page;

}
