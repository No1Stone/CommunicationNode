package com.sparwk.communicationnode.post.jpa.entity;

import com.sparwk.communicationnode.post.jpa.entity.id.ProfilePostEmotionId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_post_emotion")
@DynamicUpdate
@IdClass(ProfilePostEmotionId.class)
public class ProfilePostEmotion {
    //프로필포스트 연관관계
    @Id
    @Column(name = "profile_post_seq", nullable = true)
    private Long profilePostSeq;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "emotion_type_cd", nullable = true)
    private String emotionTypeCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfilePostEmotion(Long profilePostSeq,
                       Long profileId,
                       String emotionTypeCd,
                       String useYn,
                       Long regUsr,
                       LocalDateTime regDt,
                       Long modUsr,
                       LocalDateTime modDt
    ) {
        this.profilePostSeq = profilePostSeq;
        this.profileId = profileId;
        this.emotionTypeCd = emotionTypeCd;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

}
