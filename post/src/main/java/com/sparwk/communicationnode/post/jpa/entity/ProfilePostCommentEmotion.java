package com.sparwk.communicationnode.post.jpa.entity;

import com.sparwk.communicationnode.post.jpa.entity.id.ProfilePostCommentEmotionId;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_post_comment_emotion")
@DynamicUpdate
@IdClass(ProfilePostCommentEmotionId.class)
public class ProfilePostCommentEmotion {
    //ProfilePostComment 아이디 사용
    @Id
    @Column(name = "post_comment_seq", nullable = true)
    private Long postCommentSeq;
    @Id
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "emotion_type_cd", nullable = true)
    private String emotionTypeCd;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfilePostCommentEmotion(Long postCommentSeq,
                              Long profileId,
                              String emotionTypeCd,
                              String useYn,
                              Long regUsr,
                              LocalDateTime regDt,
                              Long modUsr,
                              LocalDateTime modDt
    ) {
        this.postCommentSeq = postCommentSeq;
        this.profileId = profileId;
        this.emotionTypeCd = emotionTypeCd;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


}
