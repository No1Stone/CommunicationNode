package com.sparwk.communicationnode.post.jpa.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostCommentEmotionBaseDTO {
    private Long postCommentSeq;
    private Long profileId;
    private String emotionTypeCd;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
