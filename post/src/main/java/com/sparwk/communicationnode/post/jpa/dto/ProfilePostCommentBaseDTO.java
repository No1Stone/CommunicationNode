package com.sparwk.communicationnode.post.jpa.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostCommentBaseDTO {
    private Long postCommentSeq;
    private Long profileId;
    private Long profilePostSeq;
    private Long upperPostCommentSeq;
    private String comment;
    private Long commentDepth;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
