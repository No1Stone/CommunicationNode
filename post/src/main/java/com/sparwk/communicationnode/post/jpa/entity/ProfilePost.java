package com.sparwk.communicationnode.post.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_post")
@DynamicUpdate
public class ProfilePost {

    @Id
    @GeneratedValue(generator = "tb_profile_post_seq")
    @Column(name = "profile_post_seq", nullable = true)
    private Long profilePostSeq;
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "title", nullable = true)
    private String title;
    @Column(name = "content", nullable = true)
    private String content;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfilePost(
            Long profilePostSeq,
            Long profileId,
            String title,
            String content,
            String useYn,
            Long regUsr,
            LocalDateTime regDt,
            Long modUsr,
            LocalDateTime modDt
    ) {
        this.profilePostSeq = profilePostSeq;
        this.profileId = profileId;
        this.title = title;
        this.content = content;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }


    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfilePostComment.class)
    @JoinColumn(name = "profile_post_seq", referencedColumnName = "profile_post_seq")
    private List<ProfilePostComment> profilePostComment;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfilePostEmotion.class)
    @JoinColumn(name = "profile_post_seq", referencedColumnName = "profile_post_seq")
    private List<ProfilePostEmotion> profilePostEmotion;
}
