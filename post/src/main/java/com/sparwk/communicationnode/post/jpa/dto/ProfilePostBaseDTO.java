package com.sparwk.communicationnode.post.jpa.dto;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostBaseDTO {

    private Long profilePostSeq;
    private Long profileId;
    private String title;
    private String content;
    private String useYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
}
