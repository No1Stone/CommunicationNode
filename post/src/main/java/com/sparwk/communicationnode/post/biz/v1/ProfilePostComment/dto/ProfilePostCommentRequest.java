package com.sparwk.communicationnode.post.biz.v1.ProfilePostComment.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostCommentRequest {
    private Long postCommentSeq;
    private Long profilePostSeq;
    private Long upperPostCommentSeq;
    private String comment;
    private Long commentDepth;
    private String useYn;
}
