package com.sparwk.communicationnode.post.jpa.repository;

import com.sparwk.communicationnode.post.jpa.entity.ProfilePostCommentEmotion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfilePostCommentEmotionRepository extends JpaRepository<ProfilePostCommentEmotion, Long> {

    List<ProfilePostCommentEmotion> findByProfileId(Long profileId);

}
