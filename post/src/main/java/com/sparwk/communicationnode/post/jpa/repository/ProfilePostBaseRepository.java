package com.sparwk.communicationnode.post.jpa.repository;

import com.sparwk.communicationnode.post.jpa.dto.ProfilePostBaseDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostCommentBaseDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostCommentEmotionBaseDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostEmotionBaseDTO;
import com.sparwk.communicationnode.post.jpa.entity.ProfilePost;
import com.sparwk.communicationnode.post.jpa.entity.ProfilePostComment;
import com.sparwk.communicationnode.post.jpa.entity.ProfilePostCommentEmotion;
import com.sparwk.communicationnode.post.jpa.entity.ProfilePostEmotion;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;
import java.util.stream.Collectors;

@Service
public class ProfilePostBaseRepository {

    @Autowired
    private ProfilePostCommentEmotionRepository profilePostCommentEmotionRepository;
    @Autowired
    private ProfilePostCommentRepository profilePostCommentRepository;
    @Autowired
    private ProfilePostEmotionRepository profilePostEmotionRepository;
    @Autowired
    private ProfilePostRepository profilePostRepository;
    @Autowired
    private ModelMapper modelMapper;

    public ProfilePostBaseDTO ProfilePostSaveService(ProfilePostBaseDTO DTO) {
        ProfilePost entity = modelMapper.map(DTO, ProfilePost.class);
        profilePostRepository.save(entity);
        ProfilePostBaseDTO result = modelMapper.map(entity, ProfilePostBaseDTO.class);
        return result;
    }

    public ProfilePostCommentBaseDTO ProfilePostCommentSaveService(ProfilePostCommentBaseDTO DTO) {
        ProfilePostComment entity = modelMapper.map(DTO, ProfilePostComment.class);
        profilePostCommentRepository.save(entity);
        ProfilePostCommentBaseDTO result = modelMapper.map(entity, ProfilePostCommentBaseDTO.class);
        return result;
    }

    public ProfilePostCommentEmotionBaseDTO ProfilePostCommentEmotionSaveService(ProfilePostCommentEmotionBaseDTO DTO) {
        ProfilePostCommentEmotion entity = modelMapper.map(DTO, ProfilePostCommentEmotion.class);
        profilePostCommentEmotionRepository.save(entity);
        ProfilePostCommentEmotionBaseDTO result = modelMapper.map(entity, ProfilePostCommentEmotionBaseDTO.class);
        return result;
    }

    public ProfilePostEmotionBaseDTO ProfilePostEmotionSaveService(ProfilePostEmotionBaseDTO DTO) {
        ProfilePostEmotion entity = modelMapper.map(DTO, ProfilePostEmotion.class);
        profilePostEmotionRepository.save(entity);
        ProfilePostEmotionBaseDTO result = modelMapper.map(entity, ProfilePostEmotionBaseDTO.class);
        return result;
    }

    public List<ProfilePostBaseDTO> ProfilePostSelectService(Long profileId) {
        return profilePostRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfilePostBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfilePostCommentBaseDTO> ProfilePostCommentSelectService(Long profileId) {
        return profilePostCommentRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfilePostCommentBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfilePostCommentEmotionBaseDTO> ProfilePostCommentEmotionSelectService(Long profileId) {
        return profilePostCommentEmotionRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfilePostCommentEmotionBaseDTO.class)).collect(Collectors.toList());
    }

    public List<ProfilePostEmotionBaseDTO> ProfilePostEmotionSelectService(Long profileId) {
        return profilePostEmotionRepository.findById(profileId).stream()
                .map(e -> modelMapper.map(e, ProfilePostEmotionBaseDTO.class)).collect(Collectors.toList());
    }


}
