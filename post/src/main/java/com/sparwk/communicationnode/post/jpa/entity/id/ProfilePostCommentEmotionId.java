package com.sparwk.communicationnode.post.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfilePostCommentEmotionId implements Serializable {
    private Long postCommentSeq;
    private Long profileId;
}
