package com.sparwk.communicationnode.post.biz.v1.ProfilePost;

import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.PostRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostComment.ProfilePostCommentService;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.ProfilePostCommentEmotionService;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion.ProfilePostEmotionService;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;

@RestController
@RequestMapping(path = "/V1/public/post")
@CrossOrigin("*")
@Api(tags = "Profile Server Post Public API")
public class ProfilePublicController {
    private final Logger logger = LoggerFactory.getLogger(ProfilePublicController.class);
    @Autowired
    private ProfilePostService profilePostService;
    @Autowired
    private ProfilePostCommentService profilePostCommentService;
    @Autowired
    private ProfilePostCommentEmotionService profilePostCommentEmotionService;
    @Autowired
    private ProfilePostEmotionService profilePostEmotionService;

    @GetMapping(path = "/info/{profileId}")
    public Response PublicProfilePostSelectController(@PathVariable(name = "profileId") Long profileId) {
        Response res = profilePostService.ProfilePostSelectService(profileId);
        return res;
    }

    @PostMapping(path = "/info/page/{profileId}")
    public Response PublicProfilePostSelectPagingController(@RequestBody PostRequest dto) {
        Response res = profilePostService.ProfilePostSelectPagingService(dto);
        return res;
    }

    @GetMapping(path = "/comment/{profileId}")
    public Response PublicProfilePostCommentSelectController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profilePostCommentService.ProfilePostPostCommentSelectService(profileId);
        return res;
    }
    @GetMapping(path = "/comment/emotion/{profileId}")
    public Response PublicProfilePostCommentEmotionController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profilePostCommentEmotionService.ProfilePostCommentEmotionSelectService(profileId);
        return res;
    }
    @GetMapping(path = "/emotion/{profileId}")
    public Response PublicProfilePostEmotionController(@PathVariable(name = "profileId")Long profileId) {
        Response res = profilePostEmotionService.ProfilePostEmotionSelectService(profileId);
        return res;
    }

}
