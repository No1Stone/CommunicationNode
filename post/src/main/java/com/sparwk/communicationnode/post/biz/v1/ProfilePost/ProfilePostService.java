package com.sparwk.communicationnode.post.biz.v1.ProfilePost;

import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.PostRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.ProfilePostRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.responsepack.ResultCodeConst;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostBaseDTO;
import com.sparwk.communicationnode.post.jpa.entity.ProfilePost;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostBaseRepository;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;
import java.util.stream.Collectors;

@Service
@RequiredArgsConstructor
public class ProfilePostService {
    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfilePostService.class);
    @Autowired
    private ProfilePostBaseRepository profilePostBaseRepository;
    @Autowired
    private ProfilePostRepository profilePostRepository;
    @Autowired
    private ModelMapper modelMapper;


    public Response ProfilePostSaveService(ProfilePostRequest dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO - {}", userInfoDTO.getAccountId());
        logger.info("userInfoDTO - {}", userInfoDTO.getLastUseProfileId());
        Response res = new Response();
        ProfilePostBaseDTO baseDTO = modelMapper.map(dto, ProfilePostBaseDTO.class);
        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setRegUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        ProfilePostBaseDTO result = profilePostBaseRepository.ProfilePostSaveService(baseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePostSelectService(Long profileId) {
        Response res = new Response();
        List<ProfilePostBaseDTO> result = profilePostRepository
                .findByProfileIdOrderByModDtDesc(profileId).stream()
                .map(e -> modelMapper.map(e, ProfilePostBaseDTO.class)).collect(Collectors.toList());
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePostSelectPagingService(PostRequest dto) {
        Response res = new Response();
        PageRequest pr = PageRequest.of(dto.getPage(), dto.getSize());

        List<Long> result = profilePostRepository
                .findByProfileIdOrderByModDtDesc(dto.getProfileId()).stream()
                .map(e -> modelMapper.map(e.getProfilePostSeq(), Long.class)).collect(Collectors.toList());

        res.setResult(profilePostRepository.findByProfilePostSeqInOrderByModDtDesc(result, pr));
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
