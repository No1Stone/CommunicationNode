package com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostRequest {

    private Long profilePostSeq;
    private String title;
    private String content;
    private String useYn;
}
