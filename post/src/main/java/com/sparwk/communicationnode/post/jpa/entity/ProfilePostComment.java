package com.sparwk.communicationnode.post.jpa.entity;

import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import java.time.LocalDateTime;
import java.util.List;

@Entity
@Getter
@Setter
@NoArgsConstructor(access = AccessLevel.PROTECTED)
@Table(name = "tb_profile_post_comment")
@DynamicUpdate
public class ProfilePostComment {

    @Id
    @GeneratedValue(generator = "tb_profile_post_comment_seq")
    @Column(name = "post_comment_seq", nullable = true)
    private Long postCommentSeq;
    @Column(name = "profile_id", nullable = true)
    private Long profileId;
    @Column(name = "profile_post_seq", nullable = true)
    private Long profilePostSeq;
    @Column(name = "upper_post_comment_seq", nullable = true)
    private Long upperPostCommentSeq;
    @Column(name = "comment", nullable = true)
    private String comment;
    @Column(name = "comment_depth", nullable = true)
    private Long commentDepth;
    @Column(name = "use_yn", nullable = true)
    private String useYn;
    @Column(name = "reg_usr", nullable = true)
    private Long regUsr;
    @Column(name = "reg_dt", nullable = true)
    private LocalDateTime regDt;
    @Column(name = "mod_usr", nullable = true)
    private Long modUsr;
    @Column(name = "mod_dt", nullable = true)
    private LocalDateTime modDt;

    @Builder
    ProfilePostComment(Long postCommentSeq,
                       Long profileId,
                       Long profilePostSeq,
                       Long upperPostCommentSeq,
                       String comment,
                       Long commentDepth,
                       String useYn,
                       Long regUsr,
                       LocalDateTime regDt,
                       Long modUsr,
                       LocalDateTime modDt
    ) {
        this.postCommentSeq = postCommentSeq;
        this.profileId = profileId;
        this.profilePostSeq = profilePostSeq;
        this.upperPostCommentSeq = upperPostCommentSeq;
        this.comment = comment;
        this.commentDepth = commentDepth;
        this.useYn = useYn;
        this.regUsr = regUsr;
        this.regDt = regDt;
        this.modUsr = modUsr;
        this.modDt = modDt;
    }

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY, targetEntity = ProfilePostCommentEmotion.class)
    @JoinColumn(name = "post_comment_seq", referencedColumnName = "post_comment_seq")
    private List<ProfilePostCommentEmotion> ㅔrofilePostCommentEmotion;
}
