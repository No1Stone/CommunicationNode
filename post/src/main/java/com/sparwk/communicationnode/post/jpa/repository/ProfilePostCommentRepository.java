package com.sparwk.communicationnode.post.jpa.repository;

import com.sparwk.communicationnode.post.jpa.entity.ProfilePostComment;
import org.springframework.data.jpa.repository.JpaRepository;

public interface ProfilePostCommentRepository extends JpaRepository<ProfilePostComment, Long> {
}
