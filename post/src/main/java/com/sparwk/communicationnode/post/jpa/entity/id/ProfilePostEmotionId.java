package com.sparwk.communicationnode.post.jpa.entity.id;

import lombok.Data;

import java.io.Serializable;

@Data
public class ProfilePostEmotionId implements Serializable {
    private Long profilePostSeq;
    private Long profileId;
}
