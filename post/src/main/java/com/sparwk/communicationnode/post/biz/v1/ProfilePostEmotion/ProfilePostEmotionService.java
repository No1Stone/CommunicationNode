package com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion;

import com.sparwk.communicationnode.post.biz.v1.ProfilePost.ProfilePostService;
import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.ProfilePostRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion.dto.ProfilePostEmotionRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.responsepack.ResultCodeConst;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostBaseDTO;
import com.sparwk.communicationnode.post.jpa.dto.ProfilePostEmotionBaseDTO;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostBaseRepository;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostEmotionRepository;
import com.sparwk.communicationnode.post.jpa.repository.ProfilePostRepository;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.support.MessageSourceAccessor;
import org.springframework.stereotype.Service;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;
import java.util.List;

@Service
@RequiredArgsConstructor
public class ProfilePostEmotionService {

    private final MessageSourceAccessor messageSourceAccessor;
    private final Logger logger = LoggerFactory.getLogger(ProfilePostEmotionService.class);
    @Autowired
    private ProfilePostBaseRepository profilePostBaseRepository;
    @Autowired
    private ProfilePostEmotionRepository profilePostEmotionRepository;
    @Autowired
    private ModelMapper modelMapper;

    public Response ProfilePostEmotionSaveService(ProfilePostEmotionRequest dto, HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO - {}", userInfoDTO.getAccountId());
        logger.info("userInfoDTO - {}", userInfoDTO.getLastUseProfileId());
        Response res = new Response();
        ProfilePostEmotionBaseDTO baseDTO = modelMapper.map(dto, ProfilePostEmotionBaseDTO.class);
        baseDTO.setProfileId(userInfoDTO.getLastUseProfileId());
        baseDTO.setModUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setRegUsr(userInfoDTO.getLastUseProfileId());
        baseDTO.setModDt(LocalDateTime.now());
        baseDTO.setRegDt(LocalDateTime.now());
        ProfilePostEmotionBaseDTO result = profilePostBaseRepository.ProfilePostEmotionSaveService(baseDTO);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }

    public Response ProfilePostEmotionSelectService(Long profileId) {
        Response res = new Response();
        List<ProfilePostEmotionBaseDTO> result = profilePostBaseRepository
                .ProfilePostEmotionSelectService(profileId);
        res.setResult(result);
        res.setResultCd(ResultCodeConst.SUCCESS.getCode());
        res.setResultMsg(messageSourceAccessor.getMessage(res.getResultCd()));
        return res;
    }
}
