package com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion.dto;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostEmotionRequest {
    private Long profilePostSeq;
    private String emotionTypeCd;
    private String useYn;

}
