package com.sparwk.communicationnode.post.jpa.repository;


import com.sparwk.communicationnode.post.jpa.entity.ProfilePostEmotion;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfilePostEmotionRepository extends JpaRepository<ProfilePostEmotion, Long> {
    List<ProfilePostEmotion> findByProfileId(Long profileId);
}
