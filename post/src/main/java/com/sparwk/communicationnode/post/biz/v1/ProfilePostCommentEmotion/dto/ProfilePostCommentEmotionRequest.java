package com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.dto;


import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class ProfilePostCommentEmotionRequest {
    private Long postCommentSeq;
    private String emotionTypeCd;
    private String useYn;
}
