package com.sparwk.communicationnode.post.jpa.repository;

import com.sparwk.communicationnode.post.jpa.entity.ProfilePost;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;

import java.util.List;

public interface ProfilePostRepository extends JpaRepository<ProfilePost, Long> {

    List<ProfilePost> findByProfileIdOrderByProfilePostSeqDesc(Long profileId);
    List<ProfilePost> findByProfileIdOrderByModDtDesc(Long profileId);

    Page<ProfilePost> findByProfilePostSeqInOrderByModDtDesc(List<Long> postSeq, Pageable Pageable);

}
