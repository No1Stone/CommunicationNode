package com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion;

import com.sparwk.communicationnode.post.biz.v1.ProfilePostComment.dto.ProfilePostCommentRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.dto.ProfilePostCommentEmotionRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/post")
@CrossOrigin("*")
@Api(tags = "Profile Server PostCommentEmotion API")
public class ProfilePostCommentEmotionController {

    private ProfilePostCommentEmotionService profilePostCommentEmotionService;
    private Logger logger = LoggerFactory.getLogger(ProfilePostCommentEmotionController.class);

    @ApiOperation(
            value = "포스트 코멘트 이모션",
            notes = "포스트 코멘트 이모션"
    )
    @PostMapping(path = "/comment/emotion")
    public Response ProfilePosttEmotionSaveController(@Valid @RequestBody ProfilePostCommentEmotionRequest dto, HttpServletRequest req) {
        logger.info("------{}",dto);
        Response res = profilePostCommentEmotionService.ProfilePostCommentEmotionSaveService(dto, req);
        return res;
    }

    @ApiOperation(
            value = "포스트 코멘트 이모션",
            notes = "포스트 코멘트 이모션"
    )
    @GetMapping(path = "/comment/emotion")
    public Response ProfilePostSelectController(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = profilePostCommentEmotionService.ProfilePostCommentEmotionSelectService(userInfoDTO.getLastUseProfileId());
        return res;
    }

}
