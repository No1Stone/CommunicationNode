package com.sparwk.communicationnode.post.biz.v1.ProfilePostComment;

import com.sparwk.communicationnode.post.biz.v1.ProfilePost.ProfilePostController;
import com.sparwk.communicationnode.post.biz.v1.ProfilePost.ProfilePostService;
import com.sparwk.communicationnode.post.biz.v1.ProfilePost.dto.ProfilePostRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostComment.dto.ProfilePostCommentRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/post")
@CrossOrigin("*")
@Api(tags = "Profile Server Post Comment API")
public class ProfilePostCommentController {
    @Autowired
    private ProfilePostCommentService profilePostCommentService;
    private final Logger logger = LoggerFactory.getLogger(ProfilePostController.class);

    @ApiOperation(
            value = "프로필 WEB URL 입력",
            notes = "web URL 입력"
    )
    @PostMapping(path = "/comment")
    public Response ProfilePostSaveController(@Valid @RequestBody ProfilePostCommentRequest dto, HttpServletRequest req) {
        logger.info("------{}",dto);
        Response res = profilePostCommentService.ProfilePostPostCommentSaveService(dto, req);
        return res;
    }

    @ApiOperation(
            value = "프로필 언어 정보 검색",
            notes = "언어 정보 검색"
    )
    @GetMapping(path = "/comment")
    public Response ProfilePostSelectController(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = profilePostCommentService.ProfilePostPostCommentSelectService(userInfoDTO.getLastUseProfileId());
        return res;
    }

}
