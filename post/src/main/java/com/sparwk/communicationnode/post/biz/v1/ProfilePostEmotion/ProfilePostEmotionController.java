package com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion;

import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.ProfilePostCommentEmotionController;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.ProfilePostCommentEmotionService;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostCommentEmotion.dto.ProfilePostCommentEmotionRequest;
import com.sparwk.communicationnode.post.biz.v1.ProfilePostEmotion.dto.ProfilePostEmotionRequest;
import com.sparwk.communicationnode.post.config.filter.responsepack.Response;
import com.sparwk.communicationnode.post.config.filter.token.UserInfoDTO;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

@RestController
@RequestMapping(path = "/V1/post")
@CrossOrigin("*")
@Api(tags = "Profile Server Post Emotion API")
public class ProfilePostEmotionController {

    private ProfilePostEmotionService profilePostEmotionService;
    private Logger logger = LoggerFactory.getLogger(ProfilePostEmotionController.class);


    @ApiOperation(
            value = "포스트 이모션",
            notes = "포스트 이모션"
    )
    @PostMapping(path = "/emotion")
    public Response ProfilePostEmotionSaveController(@Valid @RequestBody ProfilePostEmotionRequest dto, HttpServletRequest req) {
        logger.info("------{}",dto);
        Response res = profilePostEmotionService.ProfilePostEmotionSaveService(dto, req);
        return res;
    }

    @ApiOperation(
            value = "포스트 이모션",
            notes = "포스트 이모션"
    )
    @GetMapping(path = "/emotion")
    public Response ProfilePostEmotionSelectController(HttpServletRequest req) {
        UserInfoDTO userInfoDTO = (UserInfoDTO) req.getAttribute("UserInfoDTO");
        logger.info("userInfoDTO", userInfoDTO.getAccountId());
        Response res = profilePostEmotionService.ProfilePostEmotionSelectService(userInfoDTO.getLastUseProfileId());
        return res;
    }

}
