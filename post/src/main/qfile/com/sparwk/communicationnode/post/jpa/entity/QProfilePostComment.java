package com.sparwk.communicationnode.post.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfilePostComment is a Querydsl query type for ProfilePostComment
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePostComment extends EntityPathBase<ProfilePostComment> {

    private static final long serialVersionUID = -1799067947L;

    public static final QProfilePostComment profilePostComment = new QProfilePostComment("profilePostComment");

    public final StringPath comment = createString("comment");

    public final NumberPath<Long> commentDepth = createNumber("commentDepth", Long.class);

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> postCommentSeq = createNumber("postCommentSeq", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePostSeq = createNumber("profilePostSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final NumberPath<Long> upperPostCommentSeq = createNumber("upperPostCommentSeq", Long.class);

    public final StringPath useYn = createString("useYn");

    public final ListPath<ProfilePostCommentEmotion, QProfilePostCommentEmotion> ㅔrofilePostCommentEmotion = this.<ProfilePostCommentEmotion, QProfilePostCommentEmotion>createList("ㅔrofilePostCommentEmotion", ProfilePostCommentEmotion.class, QProfilePostCommentEmotion.class, PathInits.DIRECT2);

    public QProfilePostComment(String variable) {
        super(ProfilePostComment.class, forVariable(variable));
    }

    public QProfilePostComment(Path<? extends ProfilePostComment> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePostComment(PathMetadata metadata) {
        super(ProfilePostComment.class, metadata);
    }

}

