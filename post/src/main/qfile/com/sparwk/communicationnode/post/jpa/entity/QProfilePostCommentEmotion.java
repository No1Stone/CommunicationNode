package com.sparwk.communicationnode.post.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfilePostCommentEmotion is a Querydsl query type for ProfilePostCommentEmotion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePostCommentEmotion extends EntityPathBase<ProfilePostCommentEmotion> {

    private static final long serialVersionUID = 1868031942L;

    public static final QProfilePostCommentEmotion profilePostCommentEmotion = new QProfilePostCommentEmotion("profilePostCommentEmotion");

    public final StringPath emotionTypeCd = createString("emotionTypeCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> postCommentSeq = createNumber("postCommentSeq", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QProfilePostCommentEmotion(String variable) {
        super(ProfilePostCommentEmotion.class, forVariable(variable));
    }

    public QProfilePostCommentEmotion(Path<? extends ProfilePostCommentEmotion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePostCommentEmotion(PathMetadata metadata) {
        super(ProfilePostCommentEmotion.class, metadata);
    }

}

