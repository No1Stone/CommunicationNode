package com.sparwk.communicationnode.post.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;


/**
 * QProfilePostEmotion is a Querydsl query type for ProfilePostEmotion
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePostEmotion extends EntityPathBase<ProfilePostEmotion> {

    private static final long serialVersionUID = -79259439L;

    public static final QProfilePostEmotion profilePostEmotion = new QProfilePostEmotion("profilePostEmotion");

    public final StringPath emotionTypeCd = createString("emotionTypeCd");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final NumberPath<Long> profilePostSeq = createNumber("profilePostSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath useYn = createString("useYn");

    public QProfilePostEmotion(String variable) {
        super(ProfilePostEmotion.class, forVariable(variable));
    }

    public QProfilePostEmotion(Path<? extends ProfilePostEmotion> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePostEmotion(PathMetadata metadata) {
        super(ProfilePostEmotion.class, metadata);
    }

}

