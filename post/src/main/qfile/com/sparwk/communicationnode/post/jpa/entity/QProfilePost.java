package com.sparwk.communicationnode.post.jpa.entity;

import static com.querydsl.core.types.PathMetadataFactory.*;

import com.querydsl.core.types.dsl.*;

import com.querydsl.core.types.PathMetadata;
import javax.annotation.Generated;
import com.querydsl.core.types.Path;
import com.querydsl.core.types.dsl.PathInits;


/**
 * QProfilePost is a Querydsl query type for ProfilePost
 */
@Generated("com.querydsl.codegen.EntitySerializer")
public class QProfilePost extends EntityPathBase<ProfilePost> {

    private static final long serialVersionUID = -1168638934L;

    public static final QProfilePost profilePost = new QProfilePost("profilePost");

    public final StringPath content = createString("content");

    public final DateTimePath<java.time.LocalDateTime> modDt = createDateTime("modDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> modUsr = createNumber("modUsr", Long.class);

    public final NumberPath<Long> profileId = createNumber("profileId", Long.class);

    public final ListPath<ProfilePostComment, QProfilePostComment> profilePostComment = this.<ProfilePostComment, QProfilePostComment>createList("profilePostComment", ProfilePostComment.class, QProfilePostComment.class, PathInits.DIRECT2);

    public final ListPath<ProfilePostEmotion, QProfilePostEmotion> profilePostEmotion = this.<ProfilePostEmotion, QProfilePostEmotion>createList("profilePostEmotion", ProfilePostEmotion.class, QProfilePostEmotion.class, PathInits.DIRECT2);

    public final NumberPath<Long> profilePostSeq = createNumber("profilePostSeq", Long.class);

    public final DateTimePath<java.time.LocalDateTime> regDt = createDateTime("regDt", java.time.LocalDateTime.class);

    public final NumberPath<Long> regUsr = createNumber("regUsr", Long.class);

    public final StringPath title = createString("title");

    public final StringPath useYn = createString("useYn");

    public QProfilePost(String variable) {
        super(ProfilePost.class, forVariable(variable));
    }

    public QProfilePost(Path<? extends ProfilePost> path) {
        super(path.getType(), path.getMetadata());
    }

    public QProfilePost(PathMetadata metadata) {
        super(ProfilePost.class, metadata);
    }

}

