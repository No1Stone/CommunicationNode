package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Song {
    private Long songId;
    private Long originalSongId;
    private String songVersionCd;
    private Long songOwner;
    private String langCd;
    private String songTitle;
    private String songSubTitle;
    private String description;
    private String avatarImgUseYn;
    private String avatarFileUrl;
    private String songAvailYn;
    private String songStatusCd;
    private LocalDateTime songStatusModDt;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private String userDefineVersion;
}
