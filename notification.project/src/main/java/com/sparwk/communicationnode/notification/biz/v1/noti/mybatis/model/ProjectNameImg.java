package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class ProjectNameImg {
    private String projId;
    private String projTitle;
    private String avatarFileUrl;
}
