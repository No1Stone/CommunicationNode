package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class NotificationStringCreate {

    private final CompanyRepository companyRepository;
    private final NotiRepository notiRepository;
    private final ProfileRepository profileRepository;
    private final ProjectRepository projectRepository;
    private final SongRepository songRepository;


    public String Noti001Case() {
    //여권인증
        return "Verified Your Passport";
    }

    public String Noti002Case(String companyProfileId) {
        //회사 로스트 승인
        StringBuffer result = new StringBuffer();
        result.append(companyRepository.SelectOneCompany(companyProfileId).getProfileCompanyName());
        result.append(" approved you as a roaster.");
        return result.toString();
    }

    public String Noti003Case(String companyProfileId) {
        //회사 로스터 제외
        StringBuffer result = new StringBuffer();
        result.append(companyRepository.SelectOneCompany(companyProfileId).getProfileCompanyName());
        result.append(" has withdrawn you from the roster.");
        return result.toString();
    }

    public String Noti004Case() {
        //IPI Name Number 인증
        return "Spawrk approved the IPI name number.";
    }

    public String Noti005Case() {
        //메시지 도착
        return "A new message has arrived.";
    }

    public String Noti006Case() {
        //신고
        return "The reports were received by Spark.";
    }

    public String Noti007Case() {
        //Account 인증
        return "Spark approved the Company Official account.";
    }

    public String Noti008Case() {
        //Account 인증
        return "Spark approved the Group Official account.";
    }

    public String Noti009Case(String projectId) {
        //오너가 프로젝트 초대 시 (To Owner)
        StringBuffer result = new StringBuffer();
        result.append("You have been invited to the ");
        result.append(projectRepository.SelectOnePoject(projectId).getProjTitle());
        return result.toString();
    }

    public String Noti010Case(String projectId) {
        //프로젝트 지원 시 (지원자)
        StringBuffer result = new StringBuffer();
        result.append("You applied for the ");
        result.append(projectRepository.SelectOnePoject(projectId).getProjTitle());
        return result.toString();
    }

    public String Noti011Case(String projectId) {
        //프로젝트 맴버가 RSVP를 승인한 경우 (To Owner)
        StringBuffer result = new StringBuffer();
        result.append(projectRepository.SelectOnePoject(projectId).getProjTitle());
        result.append(" accepted the project invitation.");
        return result.toString();
    }

    public String Noti012Case() {
        //프로젝트 맴버가 RSVP를 거절한 경우 (To Owner) 보류
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti013Case(String profileId) {
        //프로젝트 오너가 A에서 B로 변경되었습니다. (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append("Due to the circumstances of the project owner, the project owner has been changed to ");
        //변경된 오너 프로필 아이디
        result.append(profileRepository.SelectOneProfile(profileId).getFullName());
        return result.toString();
    }

    public String Noti014Case(String profileId) {
        //맴버 탈퇴 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(profileRepository.SelectOneProfile(profileId).getFullName());
        result.append(" left the project.");
        return result.toString();
    }

    public String Noti015Case(String profileId, String projectId) {
        //맴버 추가, 비밀번호 입력 후 입장 시 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(profileRepository.SelectOneProfile(profileId).getFullName());
        result.append(" has been added as a ");
        result.append(projectRepository.SelectOnePoject(projectId).getProjTitle());
        result.append(" project member.");
        return result.toString();
    }

    public String Noti016Case() {
        //접속 시 프로젝스 지원 승인
        return "The project application has been approved.";
    }

    public String Noti017Case(String profileId, String songId) {
        //맴버 추가 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(profileRepository.SelectOneProfile(profileId).getFullName());
        result.append(" has been added as a ");
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" Co-writer member.");
        return result.toString();
    }

    public String Noti018Case(String songId) {
        //곡 생성 (Owner)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" Song has been created.");
        return result.toString();
    }

    public String Noti019Case(String songId) {
        //곡 삭제 (Owner)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" Song has been Deleted.");
        return result.toString();
    }

    public String Noti020Case() {
        //Share Split Saved (Owner & Member)
        return "The song Share Split has been saved. Check the split information.";
    }

    public String Noti021Case(String songId) {
        //스플릿 의사표시 완료(To Owner)
        StringBuffer result = new StringBuffer();
        result.append("All participating members of ");
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" have expressed their intentions.");
        return result.toString();
    }

    public String Noti022Case(String songId) {
        //Available (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" song has become available. Write a pitch list in the song library.");
        return result.toString();
    }

    public String Noti023Case(String songId, String companyProfileId) {
        //Cut 되었을 때 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" song has become Hold from ");
        result.append(companyRepository.SelectOneCompany(companyProfileId).getProfileCompanyName());
        return result.toString();
    }

    public String Noti024Case(String songId, String companyProfileId) {
        //Hold 되었을 때 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" song has become Hold from ");
        result.append(companyRepository.SelectOneCompany(companyProfileId).getProfileCompanyName());
        return result.toString();
    }

    public String Noti025Case(String songId, String companyProfileId) {
        //Pass 되었을 때 (Owner & Member)
        StringBuffer result = new StringBuffer();
        result.append(songRepository.SelectOneSong(songId).getSongTitle());
        result.append(" song has become Pass from ");
        result.append(companyRepository.SelectOneCompany(companyProfileId).getProfileCompanyName());
        return result.toString();
    }

    public String Noti026Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti027Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti028Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti029Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti030Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }

    public String Noti031Case() {
        StringBuffer result = new StringBuffer();
        return result.toString();
    }


}
