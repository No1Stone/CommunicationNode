package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProfileCompany;
import org.apache.ibatis.annotations.Mapper;


@Mapper
public interface CompanyRepository {

    ProfileCompany SelectOneCompany(String profileId);

}
