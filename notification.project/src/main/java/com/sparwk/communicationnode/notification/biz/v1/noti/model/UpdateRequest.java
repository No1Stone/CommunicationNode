package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.*;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class UpdateRequest {

    private Long profileNotiSeq;
    private Long profileId;

}
