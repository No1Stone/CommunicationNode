package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.Data;
import lombok.ToString;

import java.util.List;
import java.util.Map;

@Data
@ToString
public class ReceiveNotification {

    private String notiCode;
    private List<String> toUsers;
    private String notiOwner;
    //노티를 보낼때 보여질 메인 이미지를 검색할수 있는 PK
    private Map<String, String> data;
    //data.put("profileId", String.valueOf(repository.get("profileId")));
    //data.put("profileCompanyId", String.valueOf(repository.get("profileCompanyId")));
    //data.put("projId", String.valueOf(repository.get("projId")));
    //data.put("songId", String.valueOf(repository.get("songId")));

}
