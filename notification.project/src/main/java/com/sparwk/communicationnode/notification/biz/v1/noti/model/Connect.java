package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.Data;

@Data
public class Connect {
    private String id;
    private String userid;
    private String username;
    private String nickname;
    private String firstname;
    private String lastname;
}
