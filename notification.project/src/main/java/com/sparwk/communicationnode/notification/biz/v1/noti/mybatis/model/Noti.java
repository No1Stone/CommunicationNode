package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Noti {

    private Long profileNotiSeq;
    private Long profileId;
    private String notiCd;
    private String params;
    private String resultMsg;
    private String componetCd;
    private String componetUrl;
    private String chkYn;
    private String delYn;
    private LocalDateTime regDt;

}
