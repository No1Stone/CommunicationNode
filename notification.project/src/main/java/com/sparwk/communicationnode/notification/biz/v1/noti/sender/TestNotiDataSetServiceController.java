package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import lombok.RequiredArgsConstructor;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
public class TestNotiDataSetServiceController {

    private final Logger logger = LoggerFactory.getLogger(TestNotiDataSetServiceController.class);
    private final TestNotiDataSetService testNotiDataSetService;

    @GetMapping(path = "/sendTest001")
    public void sendTest001() {
        testNotiDataSetService.NotiTestCode001();
    }

    @GetMapping(path = "/sendTest002")
    public void sendTest002() {
        testNotiDataSetService.NotiTestCode002();
    }

    @GetMapping(path = "/sendTest003")
    public void sendTest003() {
        testNotiDataSetService.NotiTestCode003();
    }

    @GetMapping(path = "/sendTest004")
    public void sendTest004() {
        testNotiDataSetService.NotiTestCode004();
    }

    @GetMapping(path = "/sendTest005")
    public void sendTest005() {
        testNotiDataSetService.NotiTestCode005();
    }

    @GetMapping(path = "/sendTest006")
    public void sendTest006() {
        testNotiDataSetService.NotiTestCode006();
    }

    @GetMapping(path = "/sendTest007")
    public void sendTest007() {
        testNotiDataSetService.NotiTestCode007();
    }

    @GetMapping(path = "/sendTest008")
    public void sendTest008() {
        testNotiDataSetService.NotiTestCode008();
    }

    @GetMapping(path = "/sendTest009")
    public void sendTest009() {
        testNotiDataSetService.NotiTestCode009();
    }

    @GetMapping(path = "/sendTest010")
    public void sendTest010() {
        testNotiDataSetService.NotiTestCode010();
    }

    @GetMapping(path = "/sendTest011")
    public void sendTest011() {
        testNotiDataSetService.NotiTestCode011();
    }

    @GetMapping(path = "/sendTest012")
    public void sendTest012() {
        testNotiDataSetService.NotiTestCode012();
    }

    @GetMapping(path = "/sendTest013")
    public void sendTest013() {
        testNotiDataSetService.NotiTestCode013();
    }

    @GetMapping(path = "/sendTest014")
    public void sendTest014() {
        testNotiDataSetService.NotiTestCode014();
    }

    @GetMapping(path = "/sendTest015")
    public void sendTest015() {
        testNotiDataSetService.NotiTestCode015();
    }

    @GetMapping(path = "/sendTest016")
    public void sendTest016() {
        testNotiDataSetService.NotiTestCode016();
    }

    @GetMapping(path = "/sendTest017")
    public void sendTest017() {
        testNotiDataSetService.NotiTestCode017();
    }

    @GetMapping(path = "/sendTest018")
    public void sendTest018() {
        testNotiDataSetService.NotiTestCode018();
    }

    @GetMapping(path = "/sendTest019")
    public void sendTest019() {
        testNotiDataSetService.NotiTestCode019();
    }

    @GetMapping(path = "/sendTest020")
    public void sendTest020() {
        testNotiDataSetService.NotiTestCode020();
    }

    @GetMapping(path = "/sendTest021")
    public void sendTest021() {
        testNotiDataSetService.NotiTestCode021();
    }

    @GetMapping(path = "/sendTest022")
    public void sendTest022() {
        testNotiDataSetService.NotiTestCode022();
    }

    @GetMapping(path = "/sendTest023")
    public void sendTest023() {
        testNotiDataSetService.NotiTestCode023();
    }

    @GetMapping(path = "/sendTest024")
    public void sendTest024() {
        testNotiDataSetService.NotiTestCode024();
    }

    @GetMapping(path = "/sendTest025")
    public void sendTest025() {
        testNotiDataSetService.NotiTestCode025();
    }


}
