package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Profile;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProfileNameImg;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProfileRepository {
    Profile SelectOneProfile(String profileId);

}
