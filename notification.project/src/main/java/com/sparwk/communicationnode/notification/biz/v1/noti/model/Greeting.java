package com.sparwk.communicationnode.notification.biz.v1.noti.model;

public class Greeting {
    private String content;
    public Greeting(){}
    public Greeting(String content){
        this.content = content;
    }
    public String getContent(){
        return content;
    }

}
