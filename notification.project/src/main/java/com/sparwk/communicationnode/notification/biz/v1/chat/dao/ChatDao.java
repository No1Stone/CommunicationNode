package com.sparwk.communicationnode.notification.biz.v1.chat.dao;

import com.sparwk.communicationnode.notification.biz.v1.noti.model.Connect;
import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public class ChatDao {
    @Autowired
    private SqlSession sqlSession;

    public List<Connect> selectConnectList() {

        List<Connect> connectList = sqlSession.selectList("chatDao.getConnectList");

        return connectList;
    }
}
