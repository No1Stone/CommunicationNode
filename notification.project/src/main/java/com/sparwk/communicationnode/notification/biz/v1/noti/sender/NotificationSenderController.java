package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import ch.qos.logback.core.net.SyslogOutputStream;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.Greeting;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.HelloMessage;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.Notification;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.ReceiveNotification;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Noti;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Profile;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProfileCompany;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Project;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.CompanyRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProfileRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.SendTo;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.messaging.simp.annotation.SendToUser;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.util.HtmlUtils;

import java.security.Principal;
import java.util.*;

@Controller
public class NotificationSenderController {

    private final Logger logger = LoggerFactory.getLogger(NotificationSenderController.class);
    private final SimpMessagingTemplate simpMessagingTemplate;   //1
    private final Set<String> connectedUsers;     //2

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ProjectRepository projectRepository;




    public NotificationSenderController(SimpMessagingTemplate simpMessagingTemplate) {
        this.simpMessagingTemplate = simpMessagingTemplate; //1
        connectedUsers = new HashSet<>();  //2
    }

    //접속 상태 등록
    @MessageMapping("/register")  //3
//    @RequestMapping(method = RequestMethod.GET, path = "")
    @SendToUser("/queue/newMember")
    public void registerUser(String webChatUsername, @RequestParam(name = "reqid")String reqid) {
       //Set<String>
//        List<Noti>
        System.out.println("registerUser");
        if (!connectedUsers.contains(webChatUsername)) {
            connectedUsers.add(webChatUsername);
            simpMessagingTemplate.convertAndSend("/topic/newMember", webChatUsername); //4

            logger.info(" = = = webChatUsername", webChatUsername);
            //this.asyncService.onAsync();
            simpMessagingTemplate.convertAndSendToUser(webChatUsername,
                    "/msg", notificationService.SelectToProfileIdDatabase(webChatUsername));
//            return  notificationService.SelectToProfileIdDatabase(webChatUsername);
//
        } else {
//            return new HashSet<>();
            simpMessagingTemplate.convertAndSendToUser(webChatUsername,
                    "/msg", notificationService.SelectToProfileIdDatabase(webChatUsername));
//            return notificationService.SelectToProfileIdDatabase(webChatUsername);
        }
    }

    //접속 상태 해지
    @MessageMapping("/unregister")  //5
    @SendTo("/topic/disconnectedUser")
    public String unregisterUser(String webChatUsername) {
        connectedUsers.remove(webChatUsername);
        return webChatUsername;
    }


    @MessageMapping("/message")  //6
    public void greeting(Notification notification) {
        simpMessagingTemplate.convertAndSendToUser(notification.getProfileId(), "/msg", notification);
    }

    @MessageMapping("/hello")
    @SendTo("/topic/greetings")
    public Greeting greeting(HelloMessage message, Principal principal) throws Exception {
        Thread.sleep(10); // simulated delay
        logger.info("GetSession----{}",principal.getName());
        simpMessagingTemplate.setUserDestinationPrefix(message.getName());
        return new Greeting("Hello, " + HtmlUtils.htmlEscape(message.getName()));
    }


    /*
    @RequestMapping(path = "/test", method = RequestMethod.GET)
    public void test() {

        Project cc = Optional.of(projectRepository
                .SelectOnePoject("9001737")).orElse(null);
        logger.info("out put --------ccc- {}", cc);

        ProfileCompany aa = Optional.of(companyRepository
                .SelectOneCompany("10000000233")).orElse(null);
        logger.info("out put --------aaa- {}", aa);

        Profile bb = Optional.of(profileRepository
                .SelectOneProfile("10000000147")).orElse(null);
        logger.info("out put --------bbb- {}", bb);

    }
*/

}
