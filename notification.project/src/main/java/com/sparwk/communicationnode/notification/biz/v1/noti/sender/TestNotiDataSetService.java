package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.google.gson.Gson;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.ReceiveNotification;
import com.sparwk.communicationnode.notification.config.common.RestCall;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class TestNotiDataSetService {

    //RestCall Code
    /**
     public static void post(String requestURL, String jsonMessage) {
     try {
     HttpClient client = HttpClientBuilder.create().build();
     HttpPost postRequest = new HttpPost(requestURL); //POST 메소드 URL 새성
     postRequest.setHeader("Accept", "application/json");
     postRequest.setHeader("Connection", "keep-alive");
     postRequest.setHeader("Content-Type", "application/json");
     //postRequest.addHeader("Authorization", token); // token 이용시

     postRequest.setEntity(new StringEntity(jsonMessage)); //json 메시지 입력

     HttpResponse response = client.execute(postRequest);

     //Response 출력
     if (response.getStatusLine().getStatusCode() == 200) {
     ResponseHandler<String> handler = new BasicResponseHandler();
     String body = handler.handleResponse(response);
     System.out.println(body);
     } else {
     System.out.println("response is error : " + response.getStatusLine().getStatusCode());
     }
     } catch (Exception e){
     System.err.println(e.toString());
     }
     }
     */

    /** RestCall gradle
     implementation group: 'org.apache.httpcomponents', name: 'httpclient', version: '4.5.13'
     */

    /**
     Gson gradle
     implementation 'com.google.code.gson:gson:2.8.6'
     */
    /**
     * ReceiveNotification
     * <p>
     * private String notiCode;
     * private List<String> toUsers;
     * private String notiOwner;
     * //노티를 보낼때 보여질 메인 이미지를 검색할수 있는 PK
     * private Map<String, String> data;
     * //data.put("profileId", String.valueOf(repository.get("profileId")));
     * //data.put("profileCompanyId", String.valueOf(repository.get("profileCompanyId")));
     * //data.put("projId", String.valueOf(repository.get("projId")));
     * //data.put("songId", String.valueOf(repository.get("songId")));
     */

    private String Noti001 = "NOTI_001";
    private String Noti002 = "NOTI_002";
    private String Noti003 = "NOTI_003";
    private String Noti004 = "NOTI_004";
    private String Noti005 = "NOTI_005";
    private String Noti006 = "NOTI_006";
    private String Noti007 = "NOTI_007";
    private String Noti008 = "NOTI_008";
    private String Noti009 = "NOTI_009";
    private String Noti010 = "NOTI_010";
    private String Noti011 = "NOTI_011";
    private String Noti012 = "NOTI_012";
    private String Noti013 = "NOTI_013";
    private String Noti014 = "NOTI_014";
    private String Noti015 = "NOTI_015";
    private String Noti016 = "NOTI_016";
    private String Noti017 = "NOTI_017";
    private String Noti018 = "NOTI_018";
    private String Noti019 = "NOTI_019";
    private String Noti020 = "NOTI_020";
    private String Noti021 = "NOTI_021";
    private String Noti022 = "NOTI_022";
    private String Noti023 = "NOTI_023";
    private String Noti024 = "NOTI_024";
    private String Noti025 = "NOTI_025";

    //여권승인 어드민
    public void NotiTestCode001() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람 
        //Noti 001 = 여권승인 여권 프로필아이디 1개
        users.add(String.valueOf(10000000147L));

        rn.setNotiCode("NOTI_001");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //회사 로스터 승인 프로필
    public void NotiTestCode002() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 002 = 승인된 회사 로스터 프로필아이디
        users.add(String.valueOf(10000000147L));

        //메세지 또는 프로필 이미지세팅용 profileId
        key.put("profileCompanyId", String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_002");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //회사 로스터 제외 프로필
    public void NotiTestCode003() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 002 = 승인된 회사 로스터 프로필아이디
        users.add(String.valueOf(10000000147L));

        //메세지 또는 프로필 이미지세팅용 profileId
        key.put("profileCompanyId", String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_003");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //IPI Name Number 인증 어드민
    public void NotiTestCode004() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 004 = 승인된 회사 로스터 프로필아이디
        users.add(String.valueOf(10000000147L));


        rn.setNotiCode("NOTI_004");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Mattermost 메시지 도착
    public void NotiTestCode005() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 005 = 메세지 받은 프로필아이디
        users.add(String.valueOf(10000000147L));

        rn.setNotiCode("NOTI_005");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //Account 인증 컴퍼니
    public void NotiTestCode006() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 006 = 메세지 받을 컴퍼니 프로필아이디
        users.add(String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_006");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //Account 인증 그룹
    public void NotiTestCode007() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 007 = 메세지 받을 컴퍼니 프로필아이디
        users.add(String.valueOf(10000000147L));

        rn.setNotiCode("NOTI_007");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //신고
    public void NotiTestCode008() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 008 = 신고당한사람 프로필아이디
        users.add(String.valueOf(10000000147L));

        rn.setNotiCode("NOTI_008");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //오너가 프로젝트 초대 시 (To Owner)
    public void NotiTestCode009() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //Noti 009 = 초대받은사람 아이디
        users.add(String.valueOf(10000000147L));

        //오너 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //초대한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_009");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //프로젝트 지원 시 (지원자)
    public void NotiTestCode010() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너 프로필아이디
        users.add(String.valueOf(10000000147L));

        //지원자 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //지원한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_010");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //프로젝트 지원 시 (지원자)
    public void NotiTestCode011() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너 프로필아이디
        users.add(String.valueOf(10000000147L));

        //지원자 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //지원한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_011");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }



    //보류
    public void NotiTestCode012() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너 프로필아이디
        users.add(String.valueOf(10000000147L));

        rn.setNotiCode("NOTI_012");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //프로젝트 오너가 A에서 B로 변경되었습니다.
    public void NotiTestCode013() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //프로젝트 참여자들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        
        //바뀐오너 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //바뀐 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_013");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //맴버 탈퇴 (Owner & Member)
    public void NotiTestCode014() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //프로젝트 참여자들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));


        //탈퇴한사람 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //탈퇴한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_014");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //맴버 추가, 비밀번호 입력 후 입장 시 (Owner & Member)
    public void NotiTestCode015() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //프로젝트 참여자들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));


        //입장한사람 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //입장한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_015");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //프로젝스 지원 승인
    public void NotiTestCode016() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //승인받은사람(지원자)
        users.add(String.valueOf(10000000147L));


        //승인한 오너 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //승인한 프로젝트
        key.put("projId", String.valueOf(351));

        rn.setNotiCode("NOTI_016");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //쏭 맴버 추가 (Owner & Member)
    public void NotiTestCode017() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //송 멤버들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));


        //새로 추가된 송코라이터 프로필아이디
        key.put("profileId", String.valueOf(10000000147L));
        //추가된 송
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_017");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //곡 생성 (Owner)
    public void NotiTestCode018() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //송 멤버들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        //추가된 송
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_018");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


    //곡 삭제 (Owner)
    public void NotiTestCode019() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //송 멤버들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        //삭제된 송
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_019");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Share Split Saved (Owner & Member)
    //오너가 Share Split을 수정하고 저장했을 때, 모든 멤버에게 알림 발송
    public void NotiTestCode020() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //송 멤버들
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        //쉐어스플릿된송아이디
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_020");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //스플릿 의사표시 완료(To Owner)
    //모든 멤버들이 찬성했을때 오너에게 알림발송
    public void NotiTestCode021() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //송 오너
        users.add(String.valueOf(10000000147L));

        //쉐어스플릿된송아이디
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_021");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Available (Owner & Member)
    public void NotiTestCode022() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너와 멤버
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        //Available된 송아이디
        key.put("songId", String.valueOf(678));

        rn.setNotiCode("NOTI_022");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Cut 되었을 때 (Owner & Member)
    public void NotiTestCode023() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너와 멤버
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));
        users.add(String.valueOf(10000000147L));

        //Cut 송아이디
        key.put("songId", String.valueOf(678));
        key.put("profileCompanyId", String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_023");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Hold 되었을 때 (Owner & Member)
    public void NotiTestCode024() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너 한테만 ??//노티정리에 오너에게 알림전달로 작성되어있습니다.
        users.add(String.valueOf(10000000147L));


        //Hold 송아이디
        key.put("songId", String.valueOf(678));
        key.put("profileCompanyId", String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_024");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }

    //Pass 되었을 때 (Owner & Member)
    public void NotiTestCode025() {
        String URL = "http://localhost:8080/requestNotification";
        //String URL = "https://communication.sparwkdev.com/requestNotification";
        ReceiveNotification rn = new ReceiveNotification();
        Gson gson = new Gson();
        Map<String, String> key = new HashMap<>();
        List<String> users = new ArrayList<>();

        //받는사람
        //오너 한테만 ??//노티정리에 오너에게 알림전달로 작성되어있습니다.
        users.add(String.valueOf(10000000147L));

        //Pass 송아이디
        key.put("songId", String.valueOf(678));
        key.put("profileCompanyId", String.valueOf(10000000185L));

        rn.setNotiCode("NOTI_025");
        //rn.setNotiOwner();
        rn.setToUsers(users);
        rn.setData(key);
        RestCall.post(URL, gson.toJson(rn));
    }


}
