package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.*;
import org.apache.ibatis.annotations.Mapper;

import java.util.List;

@Mapper
public interface SongRepository {

    Song SelectOneSong(String songId);
    List<SongCowriter> SelectListSongCowriter(String songId);
    SongFile SelectOneSongFile(String songId);

}
