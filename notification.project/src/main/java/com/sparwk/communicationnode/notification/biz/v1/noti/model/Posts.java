package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.Data;

@Data
public class Posts {
    private String id;
    private long createat;
    private long updateat;
    private long editat;
    private long deleteat;
    private boolean ispinned;
    private String userid;
    private String channelid;
    private String rootid;
    private String parentid;
    private String originalid;
    private String message;
    private String type;
    private String props;
    private String hashtags;
    private String filenames;
    private String fileids;
    private boolean hasreactions;
    private String remoteid;
}
