package com.sparwk.communicationnode.notification.biz.v1.noti.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ProfileDao {
    @Autowired
    private SqlSession sqlSession;

    public Map<String, String> getProfileInfo(String profileId) {
        Map<String, String> profileInfo = sqlSession.selectOne("profileDao.getProfileInfo", profileId);
        return profileInfo;
    }

}
