package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.sparwk.communicationnode.notification.biz.v1.noti.model.Notification;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

public class AsyncService {

    private static final Logger logger = LoggerFactory.getLogger(AsyncService.class);
    private final SimpMessagingTemplate simpMessagingTemplate;   //1
    public AsyncService(SimpMessagingTemplate simpMessagingTemplate){
        System.out.println("AsyncService 생성자 호출");
        this.simpMessagingTemplate = simpMessagingTemplate; //1
        //
    }

    private int aIdx = 0;
    private int bIdx = 0;
    //비동기로 동작하는 메소드
    @Async
    public void onAsync() {
        while(true) {
            try {
                Thread.sleep(1000);
                logger.info("onAsync");
                System.out.println("send msg...");

                Notification notification = new Notification();
                notification.setProfileId("123");
                notification.setNotiMsg(aIdx+++"");

                Notification notification2 = new Notification();
                notification2.setProfileId("999");
                notification2.setNotiMsg(bIdx+++"");

                simpMessagingTemplate.convertAndSendToUser(notification.getProfileId(), "/msg", notification);
                simpMessagingTemplate.convertAndSendToUser(notification2.getProfileId(), "/msg", notification2);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    //동기로 동작하는 메소드
    public void onSync() {
        try {
            Thread.sleep(5000);
            logger.info("onSync");
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
