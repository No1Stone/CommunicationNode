package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongFile {

    private Long songId;
    private Long songFileSeq;
    private String songFileName;
    private String songFilePath;
    private Long songFileSize;
    private String songFileComt;
    private Long bpm;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private String containSampleYn;
    private String stereoType;
    private String bitDepth;
    private String samplingRate;


}
