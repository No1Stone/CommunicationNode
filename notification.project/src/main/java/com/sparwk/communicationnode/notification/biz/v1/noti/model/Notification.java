package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.Data;

@Data
public class Notification {
    private String profileId;
    private String notiCode;
    private String notiMsg;
    private int viewCnt;
}
