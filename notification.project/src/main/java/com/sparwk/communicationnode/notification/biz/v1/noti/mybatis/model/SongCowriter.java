package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SongCowriter {
    private Long songId;
    private Long profileId;
    private Double rateShare;
    private Long acceptYn;
    private LocalDateTime regUsr;
    private Long regDt;
    private LocalDateTime modUsr;
    private Long modDt;
    private Long songCowriterSeq;
    private String personIdType;
    private String personIdNumber;
    private Long opProfileId;
    private Long proProfileId;
    private LocalDateTime acceptDt;
    private String copyrightControlYn;
    private String rateShareComt;
}
