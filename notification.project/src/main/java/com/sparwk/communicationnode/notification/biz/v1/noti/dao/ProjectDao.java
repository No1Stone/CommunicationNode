package com.sparwk.communicationnode.notification.biz.v1.noti.dao;

import org.apache.ibatis.session.SqlSession;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;

@Repository
public class ProjectDao {
    @Autowired
    private SqlSession sqlSession;

    public Map<String, String> getProjectInfo(String projectId) {

        Map<String, String> projectInfo = sqlSession.selectOne("projectDao.getProjectInfo", projectId);

        return projectInfo;
    }
}
