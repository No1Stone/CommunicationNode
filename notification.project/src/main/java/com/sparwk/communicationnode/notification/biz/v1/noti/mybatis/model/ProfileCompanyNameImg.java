package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class ProfileCompanyNameImg {
    private String profileId;
    private String profileCompanyName;
    private String profileImgUrl;

}
