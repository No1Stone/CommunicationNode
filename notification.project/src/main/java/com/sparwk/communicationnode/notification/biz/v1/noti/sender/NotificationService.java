package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.google.gson.Gson;
import com.sparwk.communicationnode.notification.biz.v1.noti.dao.CompanyDao;
import com.sparwk.communicationnode.notification.biz.v1.noti.dao.ProfileDao;
import com.sparwk.communicationnode.notification.biz.v1.noti.dao.ProjectDao;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.Notification;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.ReceiveNotification;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.SendNotification;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.UpdateRequest;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Noti;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.CompanyRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.NotiRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProfileRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.List;

@Service
public class NotificationService {
    private static final Logger logger = LoggerFactory.getLogger(NotificationService.class);
    private final SimpMessagingTemplate simpMessagingTemplate;   //1

    @Autowired
    private ProfileDao profileDao;
    @Autowired
    private CompanyDao companyDao;
    @Autowired
    private ProjectDao projectDao;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ProjectRepository projectRepository;
    @Autowired
    private NotiRepository notiRepository;
    @Autowired
    private NotificationStringCreate notificationStringCreate;
    @Autowired
    private NotificationImgCreate notificationImgCreate;

    public NotificationService(SimpMessagingTemplate simpMessagingTemplate) {
        System.out.println("NotificationService 생성자 호출");
        this.simpMessagingTemplate = simpMessagingTemplate; //1
    }

    public void sendToUser(String notification) {
        simpMessagingTemplate.convertAndSendToUser(notification, "/msg", notification);
    }
    public void sendToUser(Notification notification) {
        simpMessagingTemplate.convertAndSendToUser(notification.getProfileId(), "/msg", notification);
    }

    public void sendToUser(SendNotification sendNotification) {
        simpMessagingTemplate.convertAndSendToUser(sendNotification.getProfileId(), "/msg", sendNotification);
    }

    public void requestNotification(ReceiveNotification receiveNotification) throws InterruptedException {
        System.out.println(receiveNotification.toString());
        Gson gson = new Gson();
        SendNotification result = new SendNotification();
        //data.put("profileId", String.valueOf(repository.get("profileId")));
        //data.put("profileCompanyId", String.valueOf(repository.get("profileCompanyId")));
        //data.put("projId", String.valueOf(repository.get("projId")));
        //data.put("songId", String.valueOf(repository.get("songId")));

        switch (receiveNotification.getNotiCode()) {
            case NotificationCodes.NOTI_001: //여권 인증
//                processNormalSystem(receiveNotification);
                result.setNotiCode(NotificationCodes.NOTI_001);
                result.setNotiMessage(notificationStringCreate.Noti001Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti001Case()));
                break;
            case NotificationCodes.NOTI_002: //회사 로스트 승인
//                processNoti002(receiveNotification);
                result.setNotiCode(NotificationCodes.NOTI_002);
                result.setNotiMessage(notificationStringCreate
                        .Noti002Case(receiveNotification.getData().get("profileCompanyId")));
                result.setParams(gson.toJson(notificationImgCreate.Noti002Case()));

                break;
            case NotificationCodes.NOTI_003: //회사 로스터 제외
                result.setNotiCode(NotificationCodes.NOTI_003);
                result.setNotiMessage(notificationStringCreate
                        .Noti003Case(receiveNotification.getData().get("profileCompanyId")));
                result.setParams(gson.toJson(notificationImgCreate.Noti003Case()));
//                processNoti003(receiveNotification);
                break;
            case NotificationCodes.NOTI_004: //IPI Name Number 인증
                result.setNotiCode(NotificationCodes.NOTI_004);
                result.setNotiMessage(notificationStringCreate.Noti004Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti004Case()));
//                processNormalSystem(receiveNotification);
                break;
            case NotificationCodes.NOTI_005: //Mattermost 메시지 도착
                result.setNotiCode(NotificationCodes.NOTI_005);
                result.setNotiMessage(notificationStringCreate.Noti005Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti005Case()));
//                processNoti005(receiveNotification);
                break;
            case NotificationCodes.NOTI_006: //Account 인증 컴퍼니
                result.setNotiCode(NotificationCodes.NOTI_006);
                result.setNotiMessage(notificationStringCreate.Noti006Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti006Case()));
                break;
            case NotificationCodes.NOTI_007: //Account 인증 그룹
                result.setNotiCode(NotificationCodes.NOTI_007);
                result.setNotiMessage(notificationStringCreate.Noti007Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti007Case()));
                break;
            case NotificationCodes.NOTI_008: //신고
                result.setNotiCode(NotificationCodes.NOTI_008);
                result.setNotiMessage(notificationStringCreate.Noti008Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti008Case()));
//                processNormalSystem(receiveNotification);
                break;
            case NotificationCodes.NOTI_009: //오너가 프로젝트 초대 시 (To Owner)
                result.setNotiCode(NotificationCodes.NOTI_009);
                result.setNotiMessage(notificationStringCreate
                        .Noti009Case(receiveNotification.getData().get("projId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti009Case(
                                receiveNotification.getData().get("profileId"),
                                receiveNotification.getData().get("projId")
                        )));
                break;
            case NotificationCodes.NOTI_010: //프로젝트 지원 시 (지원자)
                result.setNotiCode(NotificationCodes.NOTI_010);
                result.setNotiMessage(notificationStringCreate
                        .Noti010Case(receiveNotification.getData().get("projId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti010Case(receiveNotification.getData().get("profileId"))));
//                processNormalSystem(receiveNotification);
                break;
            case NotificationCodes.NOTI_011: //프로젝트 맴버가 RSVP를 승인한 경우 (To Owner)
                result.setNotiCode(NotificationCodes.NOTI_011);
                result.setNotiMessage(notificationStringCreate
                        .Noti011Case(receiveNotification.getData().get("projId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti011Case(receiveNotification.getData().get("profileId"))));
//                processNormalSystem(receiveNotification);
                break;
            //보류
            case NotificationCodes.NOTI_012: //보류
                result.setNotiCode(NotificationCodes.NOTI_012);
                result.setNotiMessage(notificationStringCreate.Noti012Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti012Case()));
//                processNoti012(receiveNotification);
                break;

            case NotificationCodes.NOTI_013: //프로젝트 오너가 A에서 B로 변경되었습니다. (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_013);
                result.setNotiMessage(notificationStringCreate
                        .Noti013Case(receiveNotification.getData().get("profileId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti013Case(receiveNotification.getData().get("profileId"))));
                break;
            case NotificationCodes.NOTI_014: //맴버 탈퇴 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_014);
                result.setNotiMessage(notificationStringCreate
                        .Noti014Case(receiveNotification.getData().get("profileId")));
                result.setParams(gson.toJson(notificationImgCreate.Noti014Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("projId")
                )));
                break;
            case NotificationCodes.NOTI_015: //맴버 추가, 비밀번호 입력 후 입장 시 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_015);
                result.setNotiMessage(notificationStringCreate.Noti015Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("projId")
                ));
                result.setParams(gson.toJson(notificationImgCreate.Noti015Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("projId")
                )));
                break;
            case NotificationCodes.NOTI_016: // 접속 시 프로젝스 지원 승인
                result.setNotiCode(NotificationCodes.NOTI_016);
                result.setNotiMessage(notificationStringCreate.Noti016Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti016Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("projId")
                )));
                break;
            case NotificationCodes.NOTI_017: // 맴버 추가 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_017);
                result.setNotiMessage(notificationStringCreate.Noti017Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("songId")
                ));
                result.setParams(gson.toJson(notificationImgCreate.Noti017Case(
                        receiveNotification.getData().get("profileId"),
                        receiveNotification.getData().get("songId")
                )));
                break;
            case NotificationCodes.NOTI_018: //곡 생성 (Owner)
                result.setNotiCode(NotificationCodes.NOTI_018);
                result.setNotiMessage(notificationStringCreate
                        .Noti018Case(receiveNotification.getData().get("songId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti018Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_019: //곡 삭제 (Owner)
                result.setNotiCode(NotificationCodes.NOTI_019);
                result.setNotiMessage(notificationStringCreate
                        .Noti019Case(receiveNotification.getData().get("songId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti019Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_020: //Share Split Saved (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_020);
                result.setNotiMessage(notificationStringCreate.Noti020Case());
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti020Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_021: //스플릿 의사표시 완료(To Owner)
                result.setNotiCode(NotificationCodes.NOTI_021);
                result.setNotiMessage(notificationStringCreate
                        .Noti021Case(receiveNotification.getData().get("songId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti021Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_022: //Available (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_022);
                result.setNotiMessage(notificationStringCreate
                        .Noti022Case(receiveNotification.getData().get("songId")));
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti022Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_023: //Cut 되었을 때 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_023);
                result.setNotiMessage(notificationStringCreate.Noti023Case(
                        receiveNotification.getData().get("songId"),
                        receiveNotification.getData().get("profileCompanyId"))
                );
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti023Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_024: //Hold 되었을 때 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_024);
                result.setNotiMessage(notificationStringCreate.Noti024Case(
                        receiveNotification.getData().get("songId"),
                        receiveNotification.getData().get("profileCompanyId"))
                );
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti024Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_025: //Pass 되었을 때 (Owner & Member)
                result.setNotiCode(NotificationCodes.NOTI_025);
                result.setNotiMessage(notificationStringCreate.Noti025Case(
                        receiveNotification.getData().get("songId"),
                        receiveNotification.getData().get("profileCompanyId"))
                );
                result.setParams(gson.toJson(notificationImgCreate
                        .Noti025Case(receiveNotification.getData().get("songId"))));
                break;
            case NotificationCodes.NOTI_026:
                result.setNotiCode(NotificationCodes.NOTI_026);
                result.setNotiMessage(notificationStringCreate.Noti026Case());
                result.setParams(gson.toJson(notificationImgCreate.Noti026Case()));
                break;
        }
        for (String e : receiveNotification.getToUsers()) {
            result.setProfileId(e);
            saveToDatabase(result);
            //세이브 쿼리 날라가서 인서트되기전에 메세지 전송이 실행될수있으니 슬립?시킬까?
            //셀렉트할때 디비를 한번더조회하니 순차적으로 처리가 될까?
            //Thread.sleep(100);
//            simpMessagingTemplate.convertAndSendToUser(e,"/msg", SelectToProfileIdDatabase(e));
            SimpleSendDatabaseData(e);
        }
    }
//        SelectToProfileIdDatabase();
//        sendToUser(sendNotification);

    public void SimpleSendDatabaseData(String profile){
        simpMessagingTemplate.convertAndSendToUser(profile,"/msg", SelectToProfileIdDatabase(profile));
    }

    /*
        private void processNoti012(ReceiveNotification receiveNotification) {
            String projectId = receiveNotification.getData().get("project_id");
            Map<String, String> project = projectDao.getProjectInfo(projectId);
            Map<String, String> data = new HashMap<>();
            data.put("project_id", String.valueOf(project.get("proj_id")));
            data.put("project_name", String.valueOf(project.get("proj_title")));

            Map<String, String> profile = profileDao.getProfileInfo(receiveNotification.getNotiOwner());

            for (String profileId : receiveNotification.getToUsers()) {
                SendNotification sendNotification = SendNotification.builder()
                        .profileId(profileId)
                        .notiCode(receiveNotification.getNotiCode())
                        .notiOwner(receiveNotification.getNotiOwner())
                        .notiImg(profile.get("profile_img_url"))
                        .isView(false)
                        .regUTCTime(String.valueOf(Instant.now().getEpochSecond()))
                        .data(data)
                        .build();
                System.out.println(sendNotification);

                saveToDatabase(sendNotification);

                sendToUser(sendNotification);
            }
        }

        private void processNormalSystem(ReceiveNotification receiveNotification) {
            for (String profileId : receiveNotification.getToUsers()) {
                SendNotification sendNotification = SendNotification.builder()
                        .profileId(profileId)
                        .notiCode(receiveNotification.getNotiCode())
                        .notiOwner(NotificationCodes.NOTI_OWNER_SYSTEN)
                        .notiImg("-")
                        .isView(false)
                        .regUTCTime(String.valueOf(Instant.now().getEpochSecond()))
                        .build();
                System.out.println(sendNotification);

                saveToDatabase(sendNotification);

                sendToUser(sendNotification);
            }
        }

        private void processNoti005(ReceiveNotification receiveNotification) {
            for (String profileId : receiveNotification.getToUsers()) {
                Map<String, String> profile = profileDao.getProfileInfo(profileId);
                Map<String, String> data = new HashMap<>();
                data.put("mattermost_id", String.valueOf(profile.get("mattermost_id")));

                SendNotification sendNotification = SendNotification.builder()
                        .profileId(profileId)
                        .notiCode(receiveNotification.getNotiCode())
                        .notiOwner(NotificationCodes.NOTI_OWNER_SYSTEN)
                        .notiImg("-")
                        .isView(false)
                        .regUTCTime(String.valueOf(Instant.now().getEpochSecond()))
                        .data(data)
                        .build();
                System.out.println(sendNotification);

                saveToDatabase(sendNotification);

                sendToUser(sendNotification);
            }
        }

        private void processNoti003(ReceiveNotification receiveNotification) {
            Map<String, String> company = companyDao.getCompanyInfo(receiveNotification.getNotiOwner());

            for (String profileId : receiveNotification.getToUsers()) {
                Map<String, String> data = new HashMap<>();
                data.put("company_id", String.valueOf(company.get("profile_id")));
                data.put("company_name", company.get("profile_company_name"));
                SendNotification sendNotification = SendNotification.builder()
                        .profileId(profileId)
                        .notiCode(receiveNotification.getNotiCode())
                        .notiOwner(receiveNotification.getNotiOwner())
                        .notiImg(company.get("profile_img_url"))
                        .isView(false)
                        .regUTCTime(String.valueOf(Instant.now().getEpochSecond()))
                        .data(data)
                        .build();
                System.out.println(sendNotification);

                saveToDatabase(sendNotification);

                sendToUser(sendNotification);
            }
        }

        private void processNoti002(ReceiveNotification receiveNotification) {
            Map<String, String> company = companyDao.getCompanyInfo(receiveNotification.getNotiOwner());

            for (String profileId : receiveNotification.getToUsers()) {
                Map<String, String> data = new HashMap<>();
                data.put("company_id", String.valueOf(company.get("profile_id")));
                data.put("company_name", company.get("profile_company_name"));
                SendNotification sendNotification = SendNotification.builder()
                        .profileId(profileId)
                        .notiCode(receiveNotification.getNotiCode())
                        .notiOwner(receiveNotification.getNotiOwner())
                        .notiImg(company.get("profile_img_url"))
                        .isView(false)
                        .regUTCTime(String.valueOf(Instant.now().getEpochSecond()))
                        .data(data)
                        .build();
                System.out.println(sendNotification);
                saveToDatabase(sendNotification);
                sendToUser(sendNotification);
            }
        }
    */
    private void saveToDatabase(SendNotification sendNotification) {
        notiRepository.NotiInsert(
                Long.parseLong(sendNotification.getProfileId()),
                sendNotification.getNotiCode(),
                sendNotification.getParams(),
                sendNotification.getNotiMessage(),
                "comCd",
                "comURL",
                "N",
                "N",
                LocalDateTime.now()
        );
    }

    public List<Noti> SelectToProfileIdDatabase(String profileId) {
        return notiRepository.SelectListNotiDelN(profileId);
    }

    public void ChkUpdateService(UpdateRequest dto) {
        notiRepository.UpdateNotiCheck(dto.getProfileNotiSeq(), dto.getProfileId());
    }

    public void DelUpdateService(UpdateRequest dto) {
        notiRepository.UpdateNotiDel(dto.getProfileNotiSeq(), dto.getProfileId());
    }
}
