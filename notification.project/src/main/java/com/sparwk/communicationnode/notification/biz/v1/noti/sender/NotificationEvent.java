package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import org.springframework.context.ApplicationEvent;

public class NotificationEvent extends ApplicationEvent {

    public NotificationEvent(Object source) {
        super(source);
    }

}
