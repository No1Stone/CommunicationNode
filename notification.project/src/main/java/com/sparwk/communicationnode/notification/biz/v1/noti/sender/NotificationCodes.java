package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

public interface NotificationCodes {
    public static String NOTI_001 = "NOTI_001";
    public static String NOTI_002 = "NOTI_002";
    public static String NOTI_003 = "NOTI_003";
    public static String NOTI_004 = "NOTI_004";
    public static String NOTI_005 = "NOTI_005";
    public static String NOTI_006 = "NOTI_006";
    public static String NOTI_007 = "NOTI_007";
    public static String NOTI_008 = "NOTI_008";
    public static String NOTI_009 = "NOTI_009";
    public static String NOTI_010 = "NOTI_010";
    public static String NOTI_011 = "NOTI_011";
    public static String NOTI_012 = "NOTI_012";
    public static String NOTI_013 = "NOTI_013";
    public static String NOTI_014 = "NOTI_014";
    public static String NOTI_015 = "NOTI_015";
    public static String NOTI_016 = "NOTI_016";
    public static String NOTI_017 = "NOTI_017";
    public static String NOTI_018 = "NOTI_018";
    public static String NOTI_019 = "NOTI_019";
    public static String NOTI_020 = "NOTI_020";
    public static String NOTI_021 = "NOTI_021";
    public static String NOTI_022 = "NOTI_022";
    public static String NOTI_023 = "NOTI_023";
    public static String NOTI_024 = "NOTI_024";
    public static String NOTI_025 = "NOTI_025";
    public static String NOTI_026 = "NOTI_026";
    public static String NOTI_027 = "NOTI_027";
    public static String NOTI_028 = "NOTI_028";
    public static String NOTI_029 = "NOTI_029";
    public static String NOTI_030 = "NOTI_030";

    public static String NOTI_OWNER_SYSTEN = "system";
}
