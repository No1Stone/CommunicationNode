package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.*;

import java.util.HashMap;
import java.util.List;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class SendToUserAndData {
    private List<String> sendUserList;
    private HashMap<String, Object> DataKVList;
}
