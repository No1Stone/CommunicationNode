package com.sparwk.communicationnode.notification.biz.v1.noti.dao;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProfileCompany;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.CompanyRepository;
import org.apache.ibatis.session.SqlSession;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import java.util.Map;
import java.util.Optional;

@Repository
public class CompanyDao
//        implements CompanyRepository
{
    @Autowired
    private SqlSession sqlSession;
    private final Logger logger = LoggerFactory.getLogger(CompanyDao.class);

    public Map<String, String> getCompanyInfo(String profileId) {
        Map<String, String> companyInfo = sqlSession.selectOne("companyDao.getCompanyInfo", profileId);
        return companyInfo;
    }

//    @Override
//    public Optional<ProfileCompany> SelectOneCompany(String profileId) {
//        logger.info("---- comDao ----- {}",profileId);
//        return Optional.of(sqlSession.selectOne("CompanyDao.SelectOneCompany", profileId));
//    }
}
