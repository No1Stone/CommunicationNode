package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.sparwk.communicationnode.notification.biz.v1.noti.model.Notification;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.stereotype.Component;

@Component
public class SenderTest implements Runnable{

    private final SimpMessagingTemplate simpMessagingTemplate;   //1
    public SenderTest(SimpMessagingTemplate simpMessagingTemplate){
        System.out.println("SenderTest 생성자 호출");
        this.simpMessagingTemplate = simpMessagingTemplate; //1
    }

    @Override
    public void run() {
        System.out.println("SenderTest run 호출");
        try {
            Thread.sleep(5000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        System.out.println("send msg...");

        Notification notification = new Notification();
        notification.setProfileId("123");
        notification.setNotiMsg("aaaaaaaaaaaaaaa");

        simpMessagingTemplate.convertAndSendToUser(notification.getProfileId(), "/msg", notification);

    }

    public void printTest(){
        System.out.println("SenderTest printTest 호출");

    }

}
