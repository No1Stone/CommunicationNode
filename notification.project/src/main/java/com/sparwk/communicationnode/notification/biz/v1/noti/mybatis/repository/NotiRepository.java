package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Noti;
import org.apache.ibatis.annotations.Mapper;

import java.time.LocalDateTime;
import java.util.List;

@Mapper
public interface NotiRepository {

    Noti SelectOneNoti(Long profileNotiSeq);

    int NotiInsert(
                   Long profileId,
                   String notiCd,
                   String params,
                   String resultMsg,
                   String componetCd,
                   String componetUrl,
                   String chkYn,
                   String delYn,
                   LocalDateTime regDt
                   );

    List<Noti> SelectListNoti(String profileId);

    List<Noti> SelectListNotiDelN(String profileId);

    int UpdateNotiCheck(Long notiSeq, Long profileId);
    int UpdateNotiDel(Long notiSeq, Long profileId);

}
