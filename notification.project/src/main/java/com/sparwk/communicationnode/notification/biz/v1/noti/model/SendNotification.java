package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.*;

import java.util.Map;

@Getter
@Setter
@Builder
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SendNotification {
    private String profileId;
    private String notiCode;
    private String notiMessage;
    private String regUTCTime;
    private String notiImg;
    private String notiOwner;
    private boolean isView;
    private String params;
    private Map<String, String> data;
}
