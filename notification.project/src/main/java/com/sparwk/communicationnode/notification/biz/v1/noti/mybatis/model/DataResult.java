package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class DataResult {
    private String type;
    private String pk;
    private String nameOrTitle;
    private String imgUrl;
}
