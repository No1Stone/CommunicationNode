package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProfileNameImg;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.Project;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.ProjectSong;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface ProjectRepository {

    Project SelectOnePoject(String prjId);
    ProfileNameImg SelectProjectJoinOnerInfo(String prjId);
    ProjectSong SelectProjectSongId(String songId);

}
