package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

import java.time.LocalDate;
import java.time.LocalDateTime;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
@ToString
public class Project {
    private Long projId;
    private Long projOwner;
    private String projAvalYn;
    private String projIndivCompType;
    private String projPublYn;
    private String projPwd;
    private String projInvtTitle;
    private String projInvtDesc;
    private String projWhatFor;
    private String pitchProjTypeCd;
    private String projWorkLocat;
    private String projTitle;
    private String projDesc;
    private LocalDateTime projDdlDt;
    private String projCondCd;
    private Long minVal;
    private Long maxVal;
    private String recruitYn;
    private String avatarFileUrl;
    private String leaveYn;
    private Long regUsr;
    private LocalDateTime regDt;
    private Long modUsr;
    private LocalDateTime modDt;
    private String genderCd;
    private String completeYn;
    private Long compProfileId;
}
