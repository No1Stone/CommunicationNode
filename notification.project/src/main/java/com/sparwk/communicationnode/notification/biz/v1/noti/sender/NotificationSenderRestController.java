package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.sparwk.communicationnode.notification.biz.v1.noti.model.ReceiveNotification;
import com.sparwk.communicationnode.notification.biz.v1.noti.model.UpdateRequest;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.CompanyRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.NotiRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProfileRepository;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.ProjectRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.messaging.simp.SimpMessagingTemplate;
import org.springframework.web.bind.annotation.*;

@RestController
@CrossOrigin("*")
public class NotificationSenderRestController {
    private final Logger logger = LoggerFactory.getLogger(NotificationSenderRestController.class);

    @Autowired
    private NotificationService notificationService;
    @Autowired
    private SimpMessagingTemplate simpMessagingTemplate;
    private NotificationSenderController notificationSenderController;

    //test
    @Autowired
    private NotiRepository notiRepository;
    @Autowired
    private CompanyRepository companyRepository;
    @Autowired
    private ProfileRepository profileRepository;
    @Autowired
    private ProjectRepository projectRepository;


//    @PostMapping(value = "/sendToUser")
//    public void sendToUser(@RequestBody Notification notification) {
//        notificationService.sendToUser(notification);
//    }

    @PostMapping(value = "/requestNotification")
    public void requestNotification(@RequestBody ReceiveNotification receiveNotification) throws InterruptedException {
        notificationService.requestNotification(receiveNotification);
    }

    @PostMapping(value = "/chkUpdate")
    public void ChkUpdateController(@RequestBody UpdateRequest dto){
        notificationService.ChkUpdateService(dto);
    }

    @PostMapping(value = "/delUpdate")
    public void DelUpdateController(@RequestBody UpdateRequest dto){
        notificationService.DelUpdateService(dto);
    }

    @GetMapping(value = "/con/{profileId}")
    public void FirstConnect(@PathVariable(name = "profileId")String profileId){
        notificationService.SimpleSendDatabaseData(profileId);
    }

}
