package com.sparwk.communicationnode.notification.biz.v1.noti.sender;

import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model.*;
import com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.repository.*;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.Optional;

@Service
@RequiredArgsConstructor
public class NotificationImgCreate {

    private final NotiRepository notiRepository;
    private final CompanyRepository companyRepository;
    private final ProfileRepository profileRepository;
    private final ProjectRepository projectRepository;
    private final SongRepository songRepository;

    private final String adminType = "admin";
    private final String adminId = "system";
    private final String adminName = "system";
    private final String adminImgURL = "https://sparwk-admin.s3.ap-northeast-2.amazonaws.com/notification_admin_system_logo_w.png";

    private DataResult getProfileNameImg(String profileId) {
        Profile profile = Optional.ofNullable(profileRepository.SelectOneProfile(profileId)).orElse(null);
        return DataResult
                .builder()
                .type("user")
                .pk(String.valueOf(profile.getProfileId()))
                .nameOrTitle(profile.getFullName())
                .imgUrl(profile.getProfileImgUrl())
                .build();
    }

    private DataResult getProfileCompanyNameImg(String profileCompany) {
        ProfileCompany pc = Optional.ofNullable(companyRepository.SelectOneCompany(profileCompany)).orElse(null);
        return DataResult
                .builder()
                .type("company")
                .pk(String.valueOf(pc.getProfileId()))
                .nameOrTitle(pc.getProfileCompanyName())
                .imgUrl(pc.getProfileImgUrl())
                .build();
    }
    private DataResult getSongOriginalId(String songId) {
        Song song = Optional.ofNullable(songRepository.SelectOneSong(songId)).orElse(null);
        return DataResult
                .builder()
                .type("songOriginId")
                .pk(String.valueOf(song.getOriginalSongId()))
                .nameOrTitle(song.getSongTitle())
                .imgUrl(song.getAvatarFileUrl())
                .build();
    }

    private DataResult getSongNameImg(String songId) {
        Song song = Optional.ofNullable(songRepository.SelectOneSong(songId)).orElse(null);
        return DataResult
                .builder()
                .type("song")
                .pk(String.valueOf(song.getSongId()))
                .nameOrTitle(song.getSongTitle())
                .imgUrl(song.getAvatarFileUrl())
                .build();
    }

    private DataResult getSongNameFileLink(String songId) {
        SongFile song = Optional.ofNullable(songRepository.SelectOneSongFile(songId)).orElse(null);
        return DataResult
                .builder()
                .type("songFile")
                .pk(String.valueOf(song.getSongId()))
                .nameOrTitle(song.getSongFileName())
                .imgUrl(song.getSongFilePath())
                .build();
    }

    private DataResult getProjectNameImg(String projId) {
        Project pj = Optional.ofNullable(projectRepository.SelectOnePoject(projId)).orElse(null);
        return DataResult
                .builder()
                .type("project")
                .pk(String.valueOf(pj.getProjId()))
                .nameOrTitle(pj.getProjTitle())
                .imgUrl(pj.getAvatarFileUrl())
                .build();
    }

    private DataResult getSongProjectId(String songId) {
        ProjectSong pj = Optional.ofNullable(projectRepository.SelectProjectSongId(songId)).orElse(null);
        Project p = Optional.ofNullable(projectRepository.SelectOnePoject(String.valueOf(pj.getProjId()))).orElse(null);
        return DataResult
                .builder()
                .type("SongProjectId")
                .pk(String.valueOf(p.getProjId()))
                .nameOrTitle(p.getProjTitle())
                .imgUrl(p.getAvatarFileUrl())
                .build();
    }

    //여권인증
    public ArrayList<DataResult> Noti001Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //회사 로스트 승인
    public ArrayList<DataResult> Noti002Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //회사로스터제외
    public ArrayList<DataResult> Noti003Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //IPI Number 인증
    public ArrayList<DataResult> Noti004Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //메세지도착
    public ArrayList<DataResult> Noti005Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //신고
    public ArrayList<DataResult> Noti006Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //Account 인증 컴퍼니
    public ArrayList<DataResult> Noti007Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //Account 인증 그룹
    public ArrayList<DataResult> Noti008Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(DataResult
                .builder()
                .type(adminType)
                .pk(adminId)
                .nameOrTitle(adminName)
                .imgUrl(adminImgURL)
                .build());
        return result;
    }

    //오너가 프로젝트 초대
    public ArrayList<DataResult> Noti009Case(String ownerId, String projectId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(ownerId));
        result.add(getProjectNameImg(projectId));
        return result;
    }

    //프로젝트 지원
    public ArrayList<DataResult> Noti010Case(String RSVPprofileId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(RSVPprofileId));
        return result;
    }

    //프로젝트 맴버가 RSVP를 승인한 경우 (To Owner)
    public ArrayList<DataResult> Noti011Case(String RSVPprofileId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(RSVPprofileId));
        return result;
    }

    //보류 프로젝트 맴버가 RSVP를 거절한 경우 (To Owner)
    public ArrayList<DataResult> Noti012Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    //프로젝트 오너가 A에서 B로 변경되었습니다. (Owner & Member)
    public ArrayList<DataResult> Noti013Case(String changageOwnerProfileId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(changageOwnerProfileId));
        return result;
    }

    //맴버 탈퇴 (Owner & Member)
    public ArrayList<DataResult> Noti014Case(String profileId, String projectId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(profileId));
        result.add(getProjectNameImg(projectId));
        return result;
    }

    //맴버 추가, 비밀번호 입력 후 입장 시 (Owner & Member)
    public ArrayList<DataResult> Noti015Case(String profileId, String projectId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(profileId));
        result.add(getProjectNameImg(projectId));
        return result;
    }

    //접속 시 프로젝스 지원 승인
    public ArrayList<DataResult> Noti016Case(String profileId, String projectId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(profileId));
        result.add(getProjectNameImg(projectId));
        return result;
    }

    //맴버 추가 (Owner & Member)
    public ArrayList<DataResult> Noti017Case(String profileId, String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getProfileNameImg(profileId));
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }

    //곡 생성 (Owner)
    public ArrayList<DataResult> Noti018Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }

    //곡 삭제 (Owner)
    public ArrayList<DataResult> Noti019Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }

    //Share Split Saved (Owner & Member)
    public ArrayList<DataResult> Noti020Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }

    //스플릿 의사표시 완료(To Owner)
    public ArrayList<DataResult> Noti021Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }

    //Available (Owner & Member)
    public ArrayList<DataResult> Noti022Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }
    //송 컷
    public ArrayList<DataResult> Noti023Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }
    //송 홀드
    public ArrayList<DataResult> Noti024Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));
        return result;
    }
    //송 패쓰
    public ArrayList<DataResult> Noti025Case(String songId) {
        ArrayList<DataResult> result = new ArrayList<>();
        result.add(getSongNameImg(songId));
        result.add(getSongOriginalId(songId));
        result.add(getSongNameFileLink(songId));
        result.add(getSongProjectId(songId));

        return result;
    }

    public ArrayList<DataResult> Noti026Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    public ArrayList<DataResult> Noti027Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    public ArrayList<DataResult> Noti028Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    public ArrayList<DataResult> Noti029Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    public ArrayList<DataResult> Noti030Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

    public ArrayList<DataResult> Noti031Case() {
        ArrayList<DataResult> result = new ArrayList<>();
        return result;
    }

}
