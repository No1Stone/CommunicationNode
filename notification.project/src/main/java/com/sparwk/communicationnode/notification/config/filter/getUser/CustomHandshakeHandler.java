package com.sparwk.communicationnode.notification.config.filter.getUser;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.http.server.ServerHttpRequest;
import org.springframework.http.server.ServerHttpResponse;
import org.springframework.http.server.ServletServerHttpRequest;
import org.springframework.web.socket.WebSocketHandler;
import org.springframework.web.socket.config.annotation.StompEndpointRegistry;
import org.springframework.web.socket.server.support.DefaultHandshakeHandler;

import javax.servlet.http.HttpSession;
import java.security.Principal;
import java.util.Map;
import java.util.UUID;

public class CustomHandshakeHandler extends DefaultHandshakeHandler {

    private final Logger logger = LoggerFactory.getLogger(CustomHandshakeHandler.class);

    @Override
    protected Principal determineUser(ServerHttpRequest request,
                                      WebSocketHandler wsHandler,
                                      Map<String, Object> attributes) {

        logger.info("request.getURI()------- {}",request.getURI());
        logger.info("request.getPrincipal()------- {}",request.getPrincipal());
        logger.info("request.getMethod()------- {}",request.getMethod());
        logger.info("request.getPrincipal()------- {}",request.getPrincipal());
        logger.info("request.getRemoteAddress()------- {}",request.getRemoteAddress());
        logger.info("wsHandler.supportsPartialMessages()------- {}",wsHandler.supportsPartialMessages());

        logger.info("request------- {}",request.toString());
        logger.info("wsHandler------- {}",wsHandler.toString());
        ServletServerHttpRequest servletRequest
                = (ServletServerHttpRequest) request;
        HttpSession session = servletRequest
                .getServletRequest().getSession();
        attributes.put("sessionId", session.getId());
        logger.info("attributes------- {}",attributes);

        return new StompPrincipal(UUID.randomUUID().toString()+request.getURI());
    }

}