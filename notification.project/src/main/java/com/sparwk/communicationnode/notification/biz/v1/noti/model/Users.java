package com.sparwk.communicationnode.notification.biz.v1.noti.model;

import lombok.Data;

@Data
public class Users {
    private String id;
    private long createat;
    private long updateat;
    private long deleteat;
    private String username;
    private String nickname;
    private String firstname;
    private String lastname;
}
