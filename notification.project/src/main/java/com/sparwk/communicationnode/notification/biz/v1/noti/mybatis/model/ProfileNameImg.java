package com.sparwk.communicationnode.notification.biz.v1.noti.mybatis.model;

import lombok.*;

@Getter@Setter@Builder
@AllArgsConstructor@NoArgsConstructor
public class ProfileNameImg {
    private String profileId;
    private String fullName;
    private String profileImgUrl;

}
